#include <iostream>
#include <string>
#include "../src/sunlight/solver.h"
#include "../src/sunlight/geometry2d.h"
#include <chrono>

using json = nlohmann::json;




int main() {

    // create save sun_puth_diagram to json file
//    CalcCelestialCoordinateTable();

    Input input;
    ReadJSONInput("../test_input/test_input_optimyzer_new_5sect.json", input, true);

//        auto start_solver = std::chrono::high_resolution_clock::now();
//    auto finish_solver = std::chrono::high_resolution_clock::now();
//    std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
//    std::cout << "CalculateWindows time: " << init_elapsed.count() << std::endl;

    // --------- create envelop
//    std::vector<double> site_envelop;
//
//    bool is_north = true;
//    PolygonByPoints site_grid;

//    CalculateSiteEnvelop(sun_path_diag, input, site_grid, site_envelop);
//    Prepare3DVisualization(input, sun_path_diag, site_grid, site_envelop);
    // --------- / create envelop

    auto start_solver = std::chrono::high_resolution_clock::now();
// ------------------- optimization task
    Output res;
    Solver(input, res, true);

    auto finish_solver = std::chrono::high_resolution_clock::now();
    //Solver(input, output, true);
    // ------------------- / optimization task

    std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
    std::cout << "init time: " << init_elapsed.count() << std::endl;


    return 0;
}



