workspace "sunlight_ws"
  configurations {"release" }


third_party = "../third_party"
libin_path = third_party .. "/libin"

function os.winSdkVersion()
	local reg_arch = iif( os.is64bit(), "\\Wow6432Node\\", "\\" )
	local sdk_version = os.getWindowsRegistry( "HKLM:SOFTWARE" .. reg_arch .."Microsoft\\Microsoft SDKs\\Windows\\v10.0\\ProductVersion" )
	if sdk_version ~= nil then return sdk_version end
end

function include_dir(dir)
	if os.istarget("windows") then
		includedirs(dir)
	elseif os.istarget("macosx") then
		sysincludedirs(dir)
	else
		includedirs(dir) end
end

function create_common_options(options)
	language "C++"
	characterset "Unicode"
	architecture "x86_64"
	exceptionhandling "On"

	cppdialect "C++14"

	location = options.project_path .. "/"

	files { options.project_path .. "/**.hpp",
			options.project_path .. "/**.h",
			options.project_path .. "/**.inl",
			options.project_path .. "/**.cpp",
			options.project_path .. "/**.c",
			location .. "**.cc"}

	filter { "configurations:debug" }
		defines { "_DEBUG" ; "_CONSOLE" }
		symbols "On"

	filter { "configurations:release" }
		defines { "NDEBUG" ; "_CONSOLE" }
		optimize "Speed"

	filter {}

end


function create_windows_options(options)
	cppdialect "C++14"
	defines { "WINDOWS", "_CRT_SECURE_NO_WARNINGS" }
	if os.winSdkVersion() ~= nil then
		systemversion(os.winSdkVersion() .. ".0")
	end
	includedirs { options.include_dirs, "../third_party/libin/protobuf/src"
        , "../third_party"
}

	libdirs {
 	"../third_party/libin/protobuf/bin/", 
	"../third_party"
}

  filter { "configurations:debug" }
	defines { "_DEBUG", "_CONSOLE", "_SCL_SECURE_NO_WARNINGS", options.defines}
    symbols "On"
	links { iif(os.istarget("windows"), "libprotobufd", "protobuf")
	}


  filter { "configurations:release" }
	defines {"NDEBUG", "_CONSOLE", "_SCL_SECURE_NO_WARNINGS", options.defines}
    optimize "Speed"
	links { iif(os.istarget("windows"), "libprotobuf", "protobuf")
	}
end


function create_linux_options(options)
	cppdialect "gnu++14"
	defines { "LINUX" }
	includedirs { options.include_dirs, "../third_party/libin/protobuf/src",
	"../third_party"
}

	libdirs {
 	"../third_party/libin/protobuf/bin/" }
	links { "pthread" }
end

function create_macosx_options(options)
	cppdialect "gnu++14"
	defines { "MACOSX" }
	sysincludedirs { options.include_dirs }
end

function build_options(options)
	options = options or {}

	create_common_options(options)

	if os.istarget("windows") then
		create_windows_options(options)
	elseif os.istarget("macosx") then
		create_macosx_options(options)
	else
		create_linux_options(options)
	end
end

 project "sunlight"
 kind "StaticLib"
 build_options { project_name="sunlight", defines={}, project_path="sunlight", include_dirs = { third_party } }
 if not os.istarget("windows") then links {"pthread",  "protobuf"} end
 if not os.istarget("windows") then buildoptions { "-fPIC", "-lX11", "-ldlib"} end

 project "sunlight_lib"
 kind "SharedLib"
 links {"sunlight"}
 build_options { project_name="sunlight_lib", defines={"PACKERLIB_EXPORTS"}, project_path="sunlight_lib", include_dirs = { third_party} }
 includedirs{ "../third_party/libin/protobuf/src" }

 libdirs { "../third_party/libin/protobuf/bin/" }
 if not os.istarget("windows") then links {"pthread",  "protobuf"} end
 if not os.istarget("windows") then buildoptions { "-fPIC" , "-lX11", "-ldlib"} end

-- project "sunlight_test_app"
-- kind "ConsoleApp"
-- links { "sunlight" }
-- build_options { project_name="sunlight_test_app", defines={}, project_path="sunlight_test_app", include_dirs = { "../third_party/libin/dlib", "../third_party"} }
-- if not os.istarget("windows") then buildoptions { "-fPIC" } end

