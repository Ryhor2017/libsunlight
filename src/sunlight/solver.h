//
// Created by ryhor on 7/4/18.
//

#ifndef PACKING2D_SOLVER_H
#define PACKING2D_SOLVER_H


#include "./proto/converter.h"
#include "settings.h"
#include "geometry2d.h"
#include "input_output.h"
#include "format3d_OFF.h"

void Solver(Input &data, Output &result, bool prepare_json_visual = false);

void InitSunPathDiagramV1(SunPathDiagramV1 &sun_path_diag);

int RayIsPassing(const int &omit_build_ind, const int &segm_numb, const Point2d &point, const double &point_height,
             const double &tg_altitude,
             const Vector2d &dir_to_sun,
             const std::vector<PolygonByPoints> &building, const std::vector<double> &building_height,
             const std::vector<PolygonByPoints> &neighb_building, const std::vector<double> &neighb_builds_heights);

struct SolutionParams {
    SolutionParams(){};
    SolutionParams(double angle_){
        angle = angle_;
    };
    Point2d origin = Point2d(0,0);
    double angle = 0;
};
struct Solution {
    Solution(){};
    Solution(const SolutionParams &params_){
        params = params_;
    };

    SolutionParams params;

    std::vector<PolygonByPoints> buildings;
    std::vector<PolygonByPoints> windows_xy;

    //std::vector<PolygonByPoints> neighb_buildings;
    //PolygonByPoints windows_x;
    std::vector<std::vector<double>> insol_by_wall;
    double insol_winter = 0;
    double insol_summer = 0;
    std::vector<std::vector<double>> insol_by_wall_winter;
    std::vector<std::vector<double>> insol_by_wall_summer;
    double insolation;



};

double CalculateInsolation(const Input &input);

void PrepareSolutionInput(const SolutionParams &params, Input &input, Solution &solution);

void CreateVisualJSON(const Solution &solution, const Input &input, const std::string &file_name = "output_test.json");

void CreateVisualJSON(const Input &input, const std::string &file_name = "output_test.json");

void CreateVisualJSONAllSolution(const std::vector<double> &insolation_by_angle,
        const std::vector<double> &min_wind_insol_by_angle, const std::string &file_name);

void TriangulateConvexPolygon(const PolygonByPoints &inp_polyg, PolygonByPoints &out_polyg);

int ReadJSONInput(const std::string &file_name, Input &input, bool read_binary = false);

void CalculateSiteEnvelop(const VirtSunPathDiagram &sun_path_diagram, const Input &input, PolygonByPoints &site_grid,
                          std::vector<double> &res);

PolygonByPoints AddBorderPoints(const PolygonByPoints &polyg, double & count);


void Prepare3DVisualization(const Input &input, const SunPathDiagramV2 &sun_path_diag,
        const PolygonByPoints &site_grid, const std::vector<double> &site_envelop,
        const std::string output_dir = kOutputDir);



#endif //PACKING2D_SOLVER_H
