//
// Created by ryhor on 29/04/19.
//

#ifndef SUNLIGHT_CELESTIAL_COORDINATE_SYSTEM_H
#define SUNLIGHT_CELESTIAL_COORDINATE_SYSTEM_H
#include "settings.h"
#include <vector>
#include <math.h>
#include <json/json.hpp>
#include <fstream>
#include "file_tools.h"


struct SunCoord {
    SunCoord() {};
    SunCoord(float azimuth_cos_, float altitude_sin_, float x_, float y_) {
        azimuth_cos = azimuth_cos_;
        altitude_sin = altitude_sin_;
        x = x_;
        y = y_;
    }
    float azimuth_cos = 0;
    float altitude_sin = 0;
    float x = 0;
    float y = 0;
};

struct InputSunCoord {
    int year_day;
    int hour;
    float latitude;
    bool is_north = true;
    InputSunCoord(const int &year_day_, const int &hour_, const float &latitude_, const bool &is_north_ = true) {
        year_day = year_day_;
        hour = hour_;
        latitude = latitude_;
        is_north = is_north_;
    }
};

class VirtSunPathDiagram {
public:
    VirtSunPathDiagram() {};

    virtual SunCoord GetSunCoord(const InputSunCoord &input) const { return SunCoord(); };

    int GetHourIndex(InputSunCoord &input_sun, const float &azim_cos, const int &x_sign) const {
        int res_hour;
        if (x_sign >= 0) {
            for (int hour = 0; hour < hour_detalization_ * 12; ++hour) {
                input_sun.hour = hour;
                auto sun_coord = GetSunCoord(input_sun);
                float azimuth_cos_next;
                if (hour != hour_detalization_ * 12 - 1) {
                    input_sun.hour = hour + 1;
                    azimuth_cos_next = GetSunCoord(input_sun).azimuth_cos;
                } else {
                    azimuth_cos_next = -1;
                }
                if (sun_coord.azimuth_cos >= azim_cos && azim_cos >= azimuth_cos_next) {
                    res_hour = hour;
                    break;
                }
            }
        } else {
            for (int hour = hour_detalization_ * 24; hour > hour_detalization_ * 12 - 1; --hour) {
                double azimuth_cos;
                if (hour == hour_detalization_ * 24) {
                    azimuth_cos = 1;
                } else {
                    input_sun.hour = hour;
                    azimuth_cos = GetSunCoord(input_sun).azimuth_cos;
                }
                float azimuth_cos_next;
                if (hour != hour_detalization_ * 12) {
                    input_sun.hour = hour - 1;
                    azimuth_cos_next = GetSunCoord(input_sun).azimuth_cos;
                } else {
                    azimuth_cos_next = -1;
                }
                if (azimuth_cos >= azim_cos && azim_cos >= azimuth_cos_next) {
                    res_hour = hour;
                    break;
                }
            }
        }
        // assert(res_hour < hour_detalization_ * 24 - 1);
        return res_hour % (hour_detalization_ * 24);
    }

    int hour_detalization_;
    int year_day_count_;
};


class SunPathDiagramV1 : public VirtSunPathDiagram {
public:
    SunPathDiagramV1() : VirtSunPathDiagram (){};
    void Init(int hour_detalization, int latitude_bin_count, int year_day_count = 365) {
        std::vector<std::vector<float>> day_hour(hour_detalization * 24, std::vector<float>(year_day_count));
        solar_sin_altitude_north.assign(latitude_bin_count, day_hour);
        solar_sin_altitude_southern.assign(latitude_bin_count, day_hour);
        solar_cos_azimuth_north.assign(latitude_bin_count, day_hour);
        solar_cos_azimuth_southern.assign(latitude_bin_count, day_hour);
        ksi_in_0_pi.assign(hour_detalization * 24, std::vector<char>(year_day_count));
        hour_detalization_ = hour_detalization;

    }
typedef std::vector<std::vector<float>> HourDay;

    void SetCoordValue(const int &latit, const int &time_ind, const int &day, const float &value,
            const bool &is_altitude, const bool &is_north) {
        std::vector<HourDay> *solar_table;
        if (is_altitude) {
            if (is_north) {
                solar_table = &solar_sin_altitude_north;
            } else {
                solar_table = &solar_sin_altitude_southern;
            }
        } else {
            if (is_north) {
                solar_table = &solar_cos_azimuth_north;
            } else {
                solar_table = &solar_cos_azimuth_southern;
            }
        }
        solar_table->at(latit).at(time_ind).at(day) = value;
    }

    void SetKsiInValue(const int &time_ind, const int &day, const char &value) {
        ksi_in_0_pi[time_ind][day] = value;
    }

    SunCoord GetSunCoord(const InputSunCoord &input) const{
        const std::vector<HourDay> *solar_sin_altitude = &solar_sin_altitude_north;
        const std::vector<HourDay> *solar_cos_azimuth = &solar_cos_azimuth_north;
        if (!input.is_north) {
            solar_sin_altitude = &solar_sin_altitude_southern;
            solar_cos_azimuth = &solar_cos_azimuth_southern;
        }
        float x, y;
        y = solar_cos_azimuth->at(input.latitude).at(input.hour).at(input.year_day);
        x = std::sqrt(1 - y * y);
        if (!ksi_in_0_pi[input.hour][input.year_day]) {
            x *= -1;
        }

        return SunCoord(solar_cos_azimuth->at(input.latitude).at(input.hour).at(input.year_day),
                solar_sin_altitude->at(input.latitude).at(input.hour).at(input.year_day), x, y);
    }

    std::vector<std::vector<char>> ksi_in_0_pi;
private:
    std::vector<HourDay> solar_sin_altitude_north;
    std::vector<HourDay> solar_sin_altitude_southern;
    std::vector<HourDay> solar_cos_azimuth_north;
    std::vector<HourDay> solar_cos_azimuth_southern;
};

class SunPathDiagramV2  : public VirtSunPathDiagram {
public:
    SunPathDiagramV2()  : VirtSunPathDiagram() {};

    void Init(const std::string &file_name_azimalt, const std::string &file_name_ksi) {
        std::ifstream fin(file_name_ksi, std::ios::binary);

        year_day_count_ = kPrecalculatedDays.size();

        fin.seekg(0, std::ifstream::end);
        int size = fin.tellg() / sizeof(char);
//        char temp2[size];
        char* temp2 = new char[size];
        fin.seekg(0, std::ifstream::beg);
//        fin.read((char *) &temp2, sizeof(temp2));
        fin.read(temp2, sizeof(char) * size);
        hour_detalization_ = temp2[0];
        int row = 0;
        int column = -1;
        ksi_in_0_pi.assign(static_cast<int>(size / year_day_count_), std::vector<char>(year_day_count_));

        for (int counter = 1; counter < size; ++counter) {
            column++;
            if (column == year_day_count_) {
                row++;
                column = 0;
            }
            ksi_in_0_pi[row][column] = temp2[counter];
        }
        fin.close();
        delete [] temp2;

        std::ifstream fin2(file_name_azimalt, std::ios::binary);

        fin2.seekg(0, std::ifstream::end);
        size = fin2.tellg() / sizeof(float);
//        float temp[size];
        float* temp = new float[size];
        fin2.seekg(0, std::ifstream::beg);
//        fin2.read((char *) &temp, sizeof(temp));
        fin2.read((char *) temp, sizeof(float) * size);
        first_hour_ind = static_cast<int>(temp[0]);
        last_hour_ind = static_cast<int>(temp[1]);
        row = 0;
        column = -1;
        solar_sin_altitude.assign(last_hour_ind - first_hour_ind + 1, std::vector<float>(year_day_count_));
        solar_cos_azimuth.assign(last_hour_ind - first_hour_ind + 1, std::vector<float>(year_day_count_));

        for (int counter = 1; counter < static_cast<int>(size / 2) - year_day_count_; ++counter) {
            column++;
            if (column == year_day_count_) {
                row++;
                column = 0;
            }
            solar_sin_altitude[row][column] = temp[counter * 2];
            solar_cos_azimuth[row][column] = temp[counter * 2 + 1];
        }

        first_hour_day_ind.assign(year_day_count_, -1);
        last_hour_day_ind.assign(year_day_count_, -1);

        row = -1;
        int base = size - 2 * year_day_count_;
        for (int counter = 0; counter < year_day_count_; ++counter) {
            row++;
            first_hour_day_ind[row] = temp[counter * 2 + base]- first_hour_ind;
            last_hour_day_ind[row] = temp[counter * 2 + 1 + base]- first_hour_ind;
            if (first_hour_day_ind[row] < 0) {
                first_hour_day_ind[row]  = 0;
                last_hour_day_ind[row]  = 0;
            }
        }

        fin2.close();
        delete [] temp;
        ksi_in_0_pi.assign(ksi_in_0_pi.begin() + first_hour_ind, ksi_in_0_pi.begin() + last_hour_ind + 1);
        return;
    }
    void CalcCelestialCoordinateTable(const std::vector<int> &year_days, const int &latitude) {
        year_day_count_ = year_days.size();
        // 1. day angle
        const double pi = 3.14159;
        std::vector<int> J(year_day_count_);
        std::vector<float> tau(year_day_count_); // day angle
        std::vector<float> delta(year_day_count_); // solar declination
        std::vector<float> ET(year_day_count_); // equation of time

        for (int i = 0; i < year_day_count_; ++i) {
            J[i] = year_days[i] + 1;
            tau[i] = 2 * pi * (J[i] - 1) / 365;
            delta[i] = 0.006918 - 0.399912 * cos(tau[i]) + 0.070257 * sin(tau[i]) - 0.006758 * cos(2 * tau[i]) +
                       0.000907 * sin(2 * tau[i]) - 0.002697 * cos(3 * tau[i]) + 0.001480 * sin(3 * tau[i]);
            ET[i] = 0.170 * sin(4 * pi * (J[i] - 80) / 373) - 0.129 * sin(2 * pi * (J[i] - 8) / 355);
        }

        // 5. true solar time
        // and 6. Hour angle
        int hour_detalization = 2; // count
        std::vector<std::vector<float>> TST(hour_detalization * 24,
                                            std::vector<float>(year_day_count_)); // equation of time
        ksi_in_0_pi.assign(hour_detalization * 24, std::vector<char>(year_day_count_));

        auto ksi = TST;

        for (int time_interval = 0; time_interval < hour_detalization * 24; ++time_interval) {
            for (int day = 0; day < year_day_count_; ++day) {
                float LT = static_cast<double>(time_interval) / hour_detalization;
                TST[time_interval][day] = LT + ET[day];
                ksi[time_interval][day] = (TST[time_interval][day] - 12) * 15 * pi / 180;
                if (ksi[time_interval][day] < 0) {
                    ksi_in_0_pi[time_interval][day] = true;
                }
            }
        }


        solar_sin_altitude.assign(hour_detalization * 24, std::vector<float>(year_day_count_));
        solar_cos_azimuth.assign(hour_detalization * 24, std::vector<float>(year_day_count_));
        int first_hour = -1, last_hour = -1;
        std::vector<int> first_hour_day(year_days.size(), -1);
        std::vector<int> last_hour_day(year_days.size(), -1);

        for (int time_interval = 0; time_interval < hour_detalization * 24; ++time_interval) {
            bool one_day_alt_posit = false;
            for (int day = 0; day < year_day_count_; ++day) {
                // ToDo latitude must be in radians
                solar_sin_altitude[time_interval][day] =
                        sin(latitude * pi / 180) * sin(delta[day]) + cos(latitude * pi / 180) * cos(delta[day]) *
                                                                     cos(ksi[time_interval][day]);

                auto sin_a = solar_sin_altitude[time_interval][day];
                auto cos_a = sqrt(1 - sin_a * sin_a);
                float res_n = kInf;
                if (cos_a > kEps) {
                    // if the sun isn't at its zenith, then azimuth is defined
                    res_n = (-sin(latitude * pi / 180) * sin_a + sin(delta[day])) /
                            (cos(latitude * pi / 180) * cos_a);
                }
                solar_cos_azimuth[time_interval][day] = res_n;

                if (solar_sin_altitude[time_interval][day] > 0) {
                    one_day_alt_posit = true;
                    if (static_cast<int>(first_hour_day[day]) == -1) {
                        first_hour_day[day] = time_interval;
                    }
                    last_hour_day[day] = time_interval;
                }
            }
            if (one_day_alt_posit) {
                if (first_hour == -1) {
                    first_hour = time_interval;
                }
                last_hour = time_interval;
            }
        }
        last_hour_ind = last_hour;
        first_hour_ind = first_hour;
        for (int j = 0; j < year_day_count_; ++j) {
            if (first_hour_day[j] == -1) { // sun doesn't rise this day
                first_hour_day[j] = 0;
                last_hour_day[j] = 0;
            } else {
                first_hour_day[j] -= first_hour;
                last_hour_day[j] -= first_hour;
            }
        }
        first_hour_day_ind = first_hour_day;
        last_hour_day_ind = last_hour_day;

        ksi_in_0_pi.assign(ksi_in_0_pi.begin() + first_hour, ksi_in_0_pi.begin() + last_hour + 1);
        solar_sin_altitude.assign(solar_sin_altitude.begin() + first_hour, solar_sin_altitude.begin() + last_hour + 1);
        solar_cos_azimuth.assign(solar_cos_azimuth.begin() + first_hour, solar_cos_azimuth.begin() + last_hour + 1);


        return;
    }

    void Init(const std::vector<int> &year_days, const int &latitude) {
        CalcCelestialCoordinateTable(year_days, latitude);
    }

    SunCoord GetSunCoord(const InputSunCoord &input) const{
        float x, y;
        y = solar_cos_azimuth.at(input.hour).at(input.year_day);
        x = std::sqrt(1 - y * y);
        if (!ksi_in_0_pi[input.hour][input.year_day]) {
            x *= -1;
        }

        return SunCoord(solar_cos_azimuth.at(input.hour).at(input.year_day),
                solar_sin_altitude.at(input.hour).at(input.year_day), x, y);
    }

    void CalculateSunCoord() {
        SunCoord sun_coord_base(-1, -1, -1, -1);
        sp_diag_local.assign(days_ind.size(),
                             std::vector<SunCoord>(last_hour_ind - first_hour_ind + 1,
                                                   sun_coord_base));

        int day_ind = -1;
        for (int day: days_ind) {
            day_ind++;
            for (int hour = first_hour_day_ind[day_ind];
                 hour < last_hour_day_ind[day_ind] + 1; ++hour) {
                sp_diag_local[day_ind][hour] = GetSunCoord(InputSunCoord(day, hour, latitude));
            }
        }
        return;
    }
    float latitude;
	std::vector<int> days_ind;
    std::vector<std::vector<SunCoord>> sp_diag_local;
    bool is_north = false;
    std::vector<std::vector<char>> ksi_in_0_pi;
    std::vector<int> first_hour_day_ind;
    std::vector<int> last_hour_day_ind;
    // local hour index
    int first_hour_ind = -1;
    int last_hour_ind = -1;
private:
    std::vector<std::vector<float>> solar_sin_altitude;
    std::vector<std::vector<float>> solar_cos_azimuth;


};
#endif //SUNLIGHT_CELESTIAL_COORDINATE_SYSTEM_H
