//
// Created by ryhor on 01/08/18.
//

#include "geometry2d.h"
#include "../../third_party/json/json.hpp"
#include "../../third_party/json/jsut.h"
#include <fstream>

using json = nlohmann::json;
using std::pair;

const double PI = 3.141592653589793238462643;

namespace dbl {

    bool Less(double x, double y) {
        return x < y - EPS;
    }

    bool Less(double x, double y, double eps) {
        return x < y - eps;
    }

    bool Equal(double x, double y) {
        return !Less(x, y) && !Less(y, x);
    }

    bool Equal(double x, double y, double eps) {
        return !Less(x, y, eps) && !Less(y, x, eps);
    }

    bool EqualLess(double x, double y, double eps) {
        return Less(x, y, eps) || Equal(x, y, eps);
    }
}

int RoundDouble(double x) {
    int res = static_cast<int>(x);
    if (dbl::Equal(static_cast<double>(res), x, kEpsForPoint2d)) {
        return res;
    }
    if (dbl::Equal(static_cast<double>(res + 1), x, kEpsForPoint2d)) {
        return res + 1;
    }
    if (dbl::Less(x, static_cast<double>(res) + 0.5)) {
        return res;
    }
    if (dbl::Less(static_cast<double>(res) + 0.5, x)) {
        return res + 1;
    }
    return res;
}

void PolygonToJSON(const PolygonByPoints &pol, const Point2d &add_point) {
    nlohmann::json res;
    nlohmann::json json_point;
    nlohmann::json points;

    for (auto point: pol.points) {
        json_point["x"] = point.x;
        json_point["y"] = point.y;
        points.push_back(json_point);
    }

    json cols_list;
    double column_width = 0.1;
    int vec_i = -1;
    for (auto point: {add_point}) {
        vec_i++;
        auto vect = Vector2d(1, 0);
        auto ort_vect = vect;
        ort_vect.x = vect.y;
        ort_vect.y = -vect.x;
        nlohmann::json edge_points, p1, p2, p3, p4, p_cent;
        auto cent = Point2d(point.x, point.y);
        p1["x"] = cent.x + column_width * vect.x + column_width * ort_vect.x;
        p1["y"] = cent.y + column_width * vect.y + column_width * ort_vect.y;

        p2["x"] = cent.x - column_width * vect.x + column_width * ort_vect.x;
        p2["y"] = cent.y - column_width * vect.y + column_width * ort_vect.y;

        p3["x"] = cent.x - column_width * vect.x - column_width * ort_vect.x;
        p3["y"] = cent.y - column_width * vect.y - column_width * ort_vect.y;

        p4["x"] = cent.x + column_width * vect.x - column_width * ort_vect.x;
        p4["y"] = cent.y + column_width * vect.y - column_width * ort_vect.y;

        p_cent["x"] = cent.x;
        p_cent["y"] = cent.y;

        edge_points.push_back(p1);
        edge_points.push_back(p_cent);
        edge_points.push_back(p2);
        edge_points.push_back(p3);
        edge_points.push_back(p_cent);
        edge_points.push_back(p4);
        cols_list.push_back(edge_points);
    }
    res["columns"] = cols_list;

    res["contour"] = points;
    std::ofstream file("output.json" + std::to_string(555));
    file << res;
    file.close();
}


int NextInd(const int &ind, const int &size) {
    return ind < size - 1 ? ind + 1 : 0;
}

bool operator == (Point2d p1, Point2d p2) {
    return dbl::Equal(p1.x, p2.x, kEpsForPoint2d) && dbl::Equal(p1.y, p2.y, kEpsForPoint2d);
}

bool Equal(const Point2d &p1, const Point2d &p2, const double & eps) {
    return dbl::Equal(p1.x, p2.x, eps) && dbl::Equal(p1.y, p2.y, eps);
}

// vector space operations
Vector2d operator * (Vector2d vect, double coef) {
    return Vector2d(vect.x * coef, vect.y * coef);
}

Point2d operator + (Point2d point, Vector2d vect) {
    return Point2d(point.x + vect.x, point.y + vect.y);
}

Vector2d operator + (Vector2d vect1, Vector2d vect2) {
    return Vector2d(vect1.x + vect2.x, vect1.y + vect2.y);
}

inline double area (Point2d a, Point2d b, Point2d c) {
	return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

inline bool intersect_1 (double a, double b, double c, double d) {
	if (dbl::Less(b, a, kEpsForPoint2d))  std::swap (a, b);
	if (dbl::Less(d, c, kEpsForPoint2d))  std::swap (c, d);
	return dbl::Less(std::max(a,c), std::min(b,d), kEpsForPoint2d) || dbl::Equal(std::max(a,c), std::min(b,d), kEpsForPoint2d);
}

inline bool intersect_1 (double a, double b, double c, double d, const double &eps) {
	if (dbl::Less(b, a, eps))  std::swap (a, b);
	if (dbl::Less(d, c, eps))  std::swap (c, d);
	return dbl::Less(std::max(a,c), std::min(b,d), eps) || dbl::Equal(std::max(a,c), std::min(b,d), eps);
}

int sign(double x) {
	if (x < - EPS) {
		return -1;
	}
	else {
		if (x > EPS) {
			return 1;
		}
	}
	return 0;
}

int sign(double x, double EPS) {
	if (x < - EPS) {
		return -1;
	}
	else {
		if (x > EPS) {
			return 1;
		}
	}
	return 0;
}

bool Intersect(const Segment &segm1, const Segment &segm2) {
    return Intersect(segm1.p1, segm1.p2, segm2.p1, segm2.p2);
}


bool IntersectSegmPolyg(const Segment &segm1, const PolygonByPoints &polyg) {
    for (int i = 0; i < polyg.points.size(); ++i) {
        int i_next = (i + 1) % polyg.points.size();
        if (Intersect(segm1, Segment(polyg.points[i], polyg.points[i_next]))) {
            return true;
        }
    }
    return false;
}

bool Intersect(const Point2d &a, const Point2d &b, const Point2d &c, const Point2d &d) {
	return intersect_1 (a.x, b.x, c.x, d.x)
		&& intersect_1 (a.y, b.y, c.y, d.y)
		&& (dbl::Less(sign(area(a,b,c), kEpsForPoint2d) * sign(area(a,b,d), kEpsForPoint2d), 0, kEpsForPoint2d) ||
		dbl::Equal(sign(area(a,b,c), kEpsForPoint2d) * sign(area(a,b,d), kEpsForPoint2d), 0, kEpsForPoint2d))
		&& (dbl::Less(sign(area(c,d,a), kEpsForPoint2d) * sign(area(c,d,b), kEpsForPoint2d), 0, kEpsForPoint2d) ||
		dbl::Equal(sign(area(c,d,a), kEpsForPoint2d) * sign(area(c,d,b), kEpsForPoint2d), 0, kEpsForPoint2d));
}

bool Intersect(const Point2d &a, const Point2d &b, const Point2d &c, const Point2d &d, const double &eps) {
	return intersect_1 (a.x, b.x, c.x, d.x, eps)
		&& intersect_1 (a.y, b.y, c.y, d.y, eps)
		&& (dbl::Less(sign(area(a,b,c), eps) * sign(area(a,b,d), eps), 0, eps) ||
		dbl::Equal(sign(area(a,b,c), eps) * sign(area(a,b,d), eps), 0, eps))
		&& (dbl::Less(sign(area(c,d,a), eps) * sign(area(c,d,b), eps), 0, eps) ||
		dbl::Equal(sign(area(c,d,a), eps) * sign(area(c,d,b), eps), 0, eps));
}

bool Intersect(const Segment &segm1, const Segment &segm2, const double &eps) {
    return Intersect(segm1.p1, segm1.p2, segm2.p1, segm2.p2, eps);
}

bool LineIntersectionSpec(const Segment &segm1, const Segment &segm2, std::vector<Point2d> &res) {
    double x1 = segm1.p1.x, y1 = segm1.p1.y;
    double x2 = segm1.p2.x, y2 = segm1.p2.y;
    double x3 = segm2.p1.x, y3 = segm2.p1.y;
    double x4 = segm2.p2.x, y4 = segm2.p2.y;
    double den = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
    double eps = 1e-3;
    // speciality - for collinear intersection return bound-points of intersection
    if (dbl::Equal(den, 0 , 0.05 )) {
        if (!VectCollinear(Vector2d(segm1), Vector2d(0, 1))) {
            std::vector<Point2d> proj_x = {segm1.p1, segm1.p2, segm2.p1, segm2.p2};
            std::sort(proj_x.begin(), proj_x.end(), [](Point2d a, Point2d b) {return a.x > b.x; });
            res = {proj_x[1]};
            if (!dbl::Equal(proj_x[1].x, proj_x[2].x, eps)) {
                res.push_back(proj_x[2]);
            }
             return true;
        } else {
            std::vector<Point2d> proj_y = {segm1.p1, segm1.p2, segm2.p1, segm2.p2};
            std::sort(proj_y.begin(), proj_y.end(), [](Point2d a, Point2d b) {return a.y > b.y; });
            res = {proj_y[1]};
            if (!dbl::Equal(proj_y[1].y, proj_y[2].y, eps)) {
                res.push_back(proj_y[2]);
            }
            return true;
        }
    }
    res = {Point2d(((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / den,
            ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / den)};
    return true;
}

bool LineIntersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4, Point2d &res) {
    double x1 = p1.x, y1 = p1.y;
    double x2 = p2.x, y2 = p2.y;
    double x3 = p3.x, y3 = p3.y;
    double x4 = p4.x, y4 = p4.y;
    double den = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
    if (dbl::Equal(den, 0 , 0.001 )) {
        return false;
        // std::cout << " Line doesn't intersect" << std::endl;
    }
    res = Point2d(((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / den,
            ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / den);
    return true;
}

bool SegmentsIntersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4, Point2d &res) {
    if (!Intersect(p1, p2, p3, p4)) {
        return false;
    }
    return LineIntersection(p1, p2, p3, p4, res);
}

PolygonBySegm ConvertPolygon(PolygonByPoints pol) {
	PolygonBySegm res;
	Point2d next_p;
	for (int i = 0; i < pol.points.size(); ++i) {
		if (i == pol.points.size() - 1) {
			next_p = pol.points[0];
		} else {
			next_p = pol.points[i + 1];
		}
		res.polygon.push_back(Segment(pol.points[i], next_p));
	}
	return res;
}

PolygonByPoints ConvertPolygon(PolygonBySegm pol) {
	PolygonByPoints res;
	for (int i = 0; i < pol.polygon.size(); ++i) {
		res.points.push_back(pol.polygon[i].p1);
	}
	return res;
}

double ScalarProduct(Vector2d v1, Vector2d v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

bool PointOnSegment(const Point2d &point, const Segment &segm) {
    if (point == segm.p1 || point == segm.p2 ) {
        return true;
    }
    Vector2d vect = Vector2d(segm);
    double det = vect.x * vect.x + vect.y * vect.y;
    double t = (1 / det) * (vect.x * (point.x - segm.p1.x)  + vect.y * (point.y - segm.p1.y));
    if (dbl::Less(t, 0, kEpsForPoint2d) || dbl::Less(1, t, kEpsForPoint2d)) {
        return false;
    } else {
        if (dbl::Less(Dist2d(point, segm.p1 + vect * t), kEpsForPoint2d)) {
            return true;
        } else {
            return false;
        }
    }
}

bool PointOnSegment(const Point2d &point, const Segment &segm, const double &eps) {
    if (point == segm.p1 || point == segm.p2 ) {
        return true;
    }
    Vector2d vect = Vector2d(segm);
    double det = vect.x * vect.x + vect.y * vect.y;
    double t = (1 / det) * (vect.x * (point.x - segm.p1.x)  + vect.y * (point.y - segm.p1.y));
    if (dbl::Less(t, 0, eps) || dbl::Less(1, t, eps)) {
        return false;
    } else {
        if (dbl::Less(Dist2d(point, segm.p1 + vect * t), eps)) {
            return true;
        } else {
            return false;
        }
    }
}

bool PointInPolygon2(const Point2d &point, const PolygonBySegm &pol) {

	Point2d p2 = point;
	p2.x += 200;
	p2.y = -pol.polygon[0].p1.y - 20;
	Segment ray(point, p2);
	auto ort_norm_ray = Norm(OrtVectOut(Vect(ray)));

    // check point on the board
    auto tmp_pol = ConvertPolygon(pol);
    int ind = -1;

    for (auto segm: pol.polygon) {
        ind++;
        if (PointOnSegment(point, segm, 0.02)) {
            return true;
        }
        if (VectCollinear(Vect(ray), Vect(segm))) {
            tmp_pol.points[ind] = tmp_pol.points[ind] + ort_norm_ray  * 10 * kEpsForPoint2d;
            tmp_pol.points[NextInd(ind, tmp_pol.points.size())] =  tmp_pol.points[NextInd(ind, tmp_pol.points.size())] + ort_norm_ray * (-10) * kEpsForPoint2d;
        }
    }
	int count_inters = 0;
    bool prev_point_on_segment = false;
    auto pol_adj = ConvertPolygon(tmp_pol);
	for (auto segm: pol_adj.polygon) {
        if (Intersect(ray.p1, ray.p2, segm.p1, segm.p2)) {
            if (PointOnSegment(segm.p1, ray) || PointOnSegment(segm.p2, ray)) {
                if (prev_point_on_segment) {
                    prev_point_on_segment = !prev_point_on_segment;
                } else {
                    prev_point_on_segment = true;
                    ++count_inters;
                }
                //std::cout << "test";
                //continue;
            } else {
                ++count_inters;
            }
        }
	}
	return count_inters % 2 == 1;
}

bool PointOnContour(const Point2d &point, const PolygonByPoints &pol) {


    // check point on the board
    auto tmp_pol = ConvertPolygon(pol);

    double eps = 0.05;

    for (auto segm: tmp_pol.polygon) {
        if (PointOnSegment(point, segm, eps)) {
            return true;
        }
    }
    return false;
}

// inside polygon and on eps-bounds of polygon
bool PointInPolygon(const Point2d &point, const PolygonBySegm &pol) {


    // check point on the board
    auto tmp_pol = ConvertPolygon(pol);

    Point2d min_p(Infin, Infin), max_p(-Infin, -Infin);

    double eps = 0.02;

    for (auto segm: pol.polygon) {
        if (PointOnSegment(point, segm, eps)) {
            return true;
        }
        min_p.x = std::min(min_p.x, segm.p1.x);
        min_p.y = std::min(min_p.y, segm.p1.y);

        max_p.x = std::max(max_p.x, segm.p1.x);
        max_p.y = std::max(max_p.y, segm.p1.y);
    }


    if (dbl::Less(point.x, min_p.x, eps) || dbl::Less(max_p.x, point.x, eps)
     || dbl::Less(point.y, min_p.y, eps) || dbl::Less(max_p.y, point.y, eps)) {
        //if (PointInPolygon2(point, pol)) {
            //return true;
        //}
        return false;
    }



	Point2d cent = min_p + Vector2d(min_p, max_p) * 0.5, nearly_corner = max_p, ray_end;
	if (max_p.x - point.x > point.x - min_p.x) {
	    nearly_corner.x = min_p.x;
	}
	if (max_p.y - point.y > point.y - min_p.y) {
	    nearly_corner.y = min_p.y;
	}


    if (max_p.x - point.x < 1) {
        nearly_corner.x = max_p.x + 1;
        nearly_corner.y = point.y;
    } else {
        if (point.x - min_p.x < 1) {
            nearly_corner.x = min_p.x - 1;
            nearly_corner.y = point.y;
        } else {
            if (max_p.y - point.y < 1) {
                nearly_corner.y = max_p.y + 1;
                nearly_corner.x = point.x;
            } else {
                if (point.y - min_p.y < 1) {
                    nearly_corner.y = min_p.y - 1;
                    nearly_corner.x = point.x;
                }
            }
        }
    }

    ray_end = nearly_corner + Norm(Vector2d(cent, nearly_corner));
	Segment ray(point, ray_end);
	auto ort_norm_ray = Norm(OrtVectOut(Vect(ray)));

    int ind = -1;
    for (auto segm: pol.polygon) {
        ind++;
        if (VectCollinear(Vect(ray), Vect(segm))) {
            tmp_pol.points[ind] = tmp_pol.points[ind] + ort_norm_ray * 10 * kEpsForPoint2d;
            tmp_pol.points[NextInd(ind, tmp_pol.points.size())] =
                    tmp_pol.points[NextInd(ind, tmp_pol.points.size())] + ort_norm_ray * (-10) * kEpsForPoint2d;
        }
    }

	int count_inters = 0;
    bool prev_point_on_segment = false;
    auto pol_adj = ConvertPolygon(tmp_pol);
	for (auto segm: pol_adj.polygon) {
        if (Intersect(ray.p1, ray.p2, segm.p1, segm.p2)) {
            if (PointOnSegment(segm.p1, ray) || PointOnSegment(segm.p2, ray)) {
                if (prev_point_on_segment) {
                    prev_point_on_segment = !prev_point_on_segment;
                } else {
                    prev_point_on_segment = true;
                    ++count_inters;
                }
                //std::cout << "test";
                //continue;
            } else {
                ++count_inters;
            }
        }
	}
//	if ((PointInPolygon2(point, pol) && !(count_inters % 2 == 1)) || (!PointInPolygon2(point, pol) && count_inters % 2 == 1)) {
//            return true;
//	}
	return count_inters % 2 == 1;
}

bool PointInPolygon(const Point2d &point, const PolygonByPoints &pol) {
    return PointInPolygon(point, ConvertPolygon(pol));
}

bool PointInPolygon(const Point2d &point, const std::vector<PolygonByPoints> &polygons) {
    for (auto &pol: polygons) {
        if (PointInPolygon(point, pol)) {
            return true;
        }
    }
    return false;
}

bool SmallRectOutPolygon(const PolygonByPoints &rect, const PolygonBySegm &pol) {
    for (auto vert: rect.points) {
        if (PointInPolygon(vert, pol)) {
            return false;
        }
    }
    return true;
}

double Dist2d(const Point2d &p1, const Point2d &p2) {
    return std::hypot(p2.x - p1.x, p2.y - p1.y);
    return std::sqrt(std::pow(p2.x - p1.x, 2) + std::pow(p2.y - p1.y, 2));
}

double Dist2d(Segment segm) {
    return Dist2d(segm.p1, segm.p2);
}

double VectorLen(Vector2d vec) {
    return std::sqrt(std::pow(vec.x, 2) + std::pow(vec.y, 2));
}

Vector2d Vect(Segment seg) {
    return Vector2d(seg.p2.x - seg.p1.x, seg.p2.y - seg.p1.y);
}

Vector2d Norm(const Vector2d &vec) {
    return Vector2d(vec.x / VectorLen(vec), vec.y / VectorLen(vec));
}

Vector2d Inverse(Vector2d vec) {
    return Vector2d(-vec.x, -vec.y);
}

// to left or to out from contour for clock wise direction
Vector2d OrtVectOut(const Vector2d &vec) {
    return Vector2d(-vec.y, vec.x);
}

PolygonBySegm CreateEnvelop(PolygonBySegm pol, double shift) {
    PolygonByPoints res;
    for (int i = 0; i < pol.polygon.size(); ++i) {
        int next_i = i + 1;
        if (next_i == pol.polygon.size()) {
            next_i = 0;
        }
        auto segm1 = pol.polygon[i];
        auto segm2 = pol.polygon[next_i];
        auto s1 = Vect(segm1);
        auto s2 = Vect(segm2);
        auto o1 = Norm(OrtVectOut(s1));
        auto o2 = Norm(OrtVectOut(s2));
        Point2d p1 = Point2d(segm1.p1.x + o1.x * shift, segm1.p1.y + o1.y * shift);
        Point2d p2 = Point2d(segm2.p1.x + o2.x * shift, segm2.p1.y + o2.y * shift);
        double det = s2.x * s1.y - s1.x * s2.y;
        if (fabs(det) < kEpsForPoint2d) {
            if (sign(s2.x * s1.x) > 0 || sign(s2.y * s1.y) > 0) {
                // res.polygon.push_back(p2);
                continue;
            }
            if (sign(s2.x * s1.x) < 0 || sign(s2.y * s1.y) < 0) {
                p1 = segm1.p2 + o1 * shift;
                p2 = segm1.p2 + o1 * (- shift);
                res.points.push_back(p1);
                res.points.push_back(p2);
                continue;
            }
        }
        double t1 = (1 / det) * (s2.x * (p2.y - p1.y) - s2.y * (p2.x - p1.x));
        Point2d res_p = Point2d(p1.x + t1 * s1.x, p1.y + t1 * s1.y);

        res.points.push_back(res_p);
    }
    return ConvertPolygon(res);
}

PolygonBySegm CreateEnvelop(PolygonBySegm pol, std::vector<double> shifts) {
    PolygonByPoints res;
    for (int i = 0; i < pol.polygon.size(); ++i) {
        int next_i = i + 1;
        if (next_i == pol.polygon.size()) {
            next_i = 0;
        }
        auto segm1 = pol.polygon[i];
        auto segm2 = pol.polygon[next_i];
        auto s1 = Vect(segm1);
        auto s2 = Vect(segm2);
        auto o1 = Norm(OrtVectOut(s1));
        auto o2 = Norm(OrtVectOut(s2));
        Point2d p1 = segm1.p1 + o1 * shifts[i];
        Point2d p2 = segm2.p1 + o2 * shifts[next_i];
        double det = s2.x * s1.y - s1.x * s2.y;
        if (fabs(det) < 10*EPS) {
            if (sign(s2.x * s1.x) > 0 || sign(s2.y * s1.y) > 0) {
                // res.polygon.push_back(p2);
                continue;
            }
            if (sign(s2.x * s1.x) < 0 || sign(s2.y * s1.y) < 0) {
                p1 = segm1.p2 + o1 * shifts[i];
                p2 = segm1.p2 + o1 * (- shifts[i]);
                res.points.push_back(p1);
                res.points.push_back(p2);
                continue;
            }
        }
        double t1 = (1 / det) * (s2.x * (p2.y - p1.y) - s2.y * (p2.x - p1.x));
        Point2d res_p = Point2d(p1.x + t1 * s1.x, p1.y + t1 * s1.y);

        res.points.push_back(res_p);
    }
    return ConvertPolygon(res);
}

void CreateEnvelop(PolygonByPoints &pol, double shift) {
    for (int i = 0; i < pol.points.size(); ++i) {
        int next_i = (i + 1) % pol.points.size();
        int next2_i = (i + 2) % pol.points.size();
        auto segm1 = Segment(pol.points[i], pol.points[next_i]);
        auto segm2 = Segment(pol.points[next_i], pol.points[next2_i]);
        auto s1 = Vect(segm1);
        auto s2 = Vect(segm2);
        auto o1 = Norm(OrtVectOut(s1));
        auto o2 = Norm(OrtVectOut(s2));
        Point2d p1 = Point2d(segm1.p1.x + o1.x * shift, segm1.p1.y + o1.y * shift);
        Point2d p2 = Point2d(segm2.p1.x + o2.x * shift, segm2.p1.y + o2.y * shift);
        double det = s2.x * s1.y - s1.x * s2.y;
        if (fabs(det) < kEpsForPoint2d) {
            if (sign(s2.x * s1.x) > 0 || sign(s2.y * s1.y) > 0) {
                // res.polygon.push_back(p2);
                continue;
            }
            if (sign(s2.x * s1.x) < 0 || sign(s2.y * s1.y) < 0) {
                p1 = segm1.p2 + o1 * shift;
                p2 = segm1.p2 + o1 * (- shift);
                continue;
            }
        }
        double t1 = (1 / det) * (s2.x * (p2.y - p1.y) - s2.y * (p2.x - p1.x));
        Point2d res_p = Point2d(p1.x + t1 * s1.x, p1.y + t1 * s1.y);
        pol.points[next_i] = res_p;
    }
}

PolygonByPoints CreateEnvelop(PolygonByPoints pol, std::vector<double> shifts) {
    return ConvertPolygon(CreateEnvelop(ConvertPolygon(pol), shifts));
}

PolygonByPoints CreateEnvelop(PolygonByPoints pol, double shifts) {
    return ConvertPolygon(CreateEnvelop(ConvertPolygon(pol), shifts));
}

bool CheckClockwise(PolygonBySegm pol, const double &shift) {
    if (pol.polygon.size() < 3) {
        return false;
    }
    int fls = 0, tr = 0;
    for (int i: {0, 1, 2}) {
        auto main_segm = pol.polygon[i];
        Point2d center((main_segm.p1.x + main_segm.p2.x) / 2, (main_segm.p1.y + main_segm.p2.y) / 2);
        auto ort = Norm(OrtVectOut(Vect(main_segm)));
        Point2d point(center.x + ort.x * shift, center.y + ort.y * shift);
        if (PointInPolygon(point, pol)) {
            fls++;
        } else {
            tr++;
        }
    }
    return tr > fls;
}



bool AngleLess90(Vector2d v1, Vector2d v2) {
    return ScalarProduct(Norm(v1), Norm(v2)) > EPS ;
}

// for clock wise direction
bool CheckInnerCornerGeo(Point2d p1, Point2d p2, Point2d p3) {
    Vector2d v1 = OrtVectOut(Vect(Segment(p1, p2)));
    Vector2d v2 = Vect(Segment(p2, p3));
    return AngleLess90(v1, v2);
}

// for clock wise direction
bool CheckOuterCornerGeo(Point2d p1, Point2d p2, Point2d p3) {
    Vector2d v1 = Inverse(OrtVectOut(Vect(Segment(p1, p2))));
    Vector2d v2 = Vect(Segment(p2, p3));
    return AngleLess90(v1, v2);
}

double CosBetwVect(Vector2d vect2D_1, Vector2d vect2D_2) {
    auto res = ScalarProduct(vect2D_1, vect2D_2) / (VectorLen(vect2D_1) * VectorLen(vect2D_2));
    if (res > 1) {
        res = 1;
    } else {
        if (res < -1) {
            res = -1;
        }
    }
    return res;
}

bool VectAntiCollinear(Vector2d vect2D_1, Vector2d vect2D_2) {
    return dbl::Equal(CosBetwVect(vect2D_1, vect2D_2), -1, kEpsForPoint2d);
}

bool VectCollinear(Vector2d vect2D_1, Vector2d vect2D_2) {
    return dbl::Equal(fabs(CosBetwVect(vect2D_1, vect2D_2)), 1, kEpsForPoint2d);
}

double OrientAngle(Vector2d vect1, Vector2d vect2) {
    // calculate angle from vect1 to vect2 in clock wise direction in degree
    if (vect1.x * vect2.y - vect1.y * vect2.x <= 0) {
        return  acos(CosBetwVect(vect1, vect2)) * 180.0 / PI;
    } else {
        return 360 - acos(CosBetwVect(vect1, vect2)) * 180.0 / PI;
    }
}

double SignTriangleArea (Point2d p1, Point2d p2, Point2d p3) {
	return (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
}

double ConvertSignArea (double sign_area) {
	return std::fabs(sign_area) / 2.0;
}

double PolygonArea(const PolygonBySegm &pol) {
    double res = 0;
    Point2d origin(0, 0);
    for (auto segm: pol.polygon) {
        res += SignTriangleArea (segm.p1, segm.p2, origin);
    }
    return ConvertSignArea(res);
 }

bool SegmentsListIntersect(const std::vector<Segment> &list1, const std::vector<Segment> &list2) {
    for (auto segm1: list1) {
        for (auto segm2: list2) {
            if (Intersect(segm1.p1, segm1.p2, segm2.p1, segm2.p2)) {
                return true;
            }
        }
    }
    return false;
}

bool CoveredBy(const PolygonByPoints &pol_in, const PolygonByPoints &pol_out) {
    auto pol_out_segm = ConvertPolygon(pol_out);
    auto pol_in_segm = ConvertPolygon(pol_in);
    for (auto point: pol_in.points) {
        if (!PointInPolygon(point, pol_out_segm)) {
            return false;
        }
    }
    if (SegmentsListIntersect(pol_in_segm.polygon, pol_out_segm.polygon)) {
        return false;
    }
    return true;
}

bool PolygonsIntersect(const PolygonByPoints &pol1, const PolygonByPoints &pol2) {
    if (CoveredBy(pol1, pol2) || CoveredBy(pol2, pol1)) {
        return true;
    }
    auto pol1_segm = ConvertPolygon(pol1);
    auto pol2_segm = ConvertPolygon(pol2);
    if (SegmentsListIntersect(pol1_segm.polygon, pol2_segm.polygon)) {
        return true;
    }
    return false;
}

bool PolygonsIntersectSpecial(const PolygonByPoints &pol1, const PolygonByPoints &pol2) {
    // any polygon doesn't cover other polygon
    auto pol1_segm = ConvertPolygon(pol1);
    auto pol2_segm = ConvertPolygon(pol2);
    if (SegmentsListIntersect(pol1_segm.polygon, pol2_segm.polygon)) {
        return true;
    }
    return false;
}

int NextIndex(int curr_index, int vect_size) {
    if (curr_index < vect_size - 1) {
        return curr_index + 1;
    } else {
        return 0;
    }
}

double IntersectionRectPolygArea(const PolygonByPoints &rect, const PolygonByPoints &pol2) {
    if (!PolygonsIntersect(rect, pol2)) {
        return 0;
    }
    if (CoveredBy(rect, pol2)) {
        return PolygonArea(ConvertPolygon(rect));
    }
    if (CoveredBy(pol2, rect)) {
        return PolygonArea(ConvertPolygon(pol2));
    }
    auto pol2_segm = ConvertPolygon(pol2);

    int grid_size = 10000;
    int max_count_points = 200;
    int count_points_in_pol = 0;
    double width = std::fabs(rect.points[0].x - rect.points[2].x);
    double height = std::fabs(rect.points[0].y - rect.points[2].y);
    Point2d origin(std::min(rect.points[0].x, rect.points[2].x), std::min(rect.points[0].y, rect.points[2].y));
    for (int i = 0; i < max_count_points; ++i) {
        int randix = (std::rand() % grid_size) + 1;
        int randiy = (std::rand() % grid_size) + 1;
        Point2d random_point(origin.x + randix * width / grid_size, origin.y + randiy * height / grid_size);
        if (PointInPolygon(random_point, pol2_segm)) {
            ++count_points_in_pol;
        }
    }
    return width * height * count_points_in_pol / max_count_points;

}

std::vector<std::vector<double>> MatrixProduct(std::vector<std::vector<double>> A, std::vector<std::vector<double>> B) {

    std::vector<std::vector<double>> prod(A.size(), std::vector<double>(B[0].size()));

    for (int i = 0; i < A.size(); i++) {
        for (int j = 0; j < B[0].size(); j++) {
            for (int k = 0; k < A[0].size(); k++) {
                prod[i][j] = A[i][k] * B[k][j];
            }
        }
    }
    return prod;
}

Point2d RotatePointOrigin(Point2d point, double alpha) {
    return Point2d(cos(alpha * PI / 180.0) * point.x - sin(alpha * PI / 180.0) * point.y,
            sin(alpha * PI / 180.0) * point.x + cos(alpha * PI / 180.0) * point.y);
}

PolygonByPoints RotatePolygonOrigin(const PolygonByPoints &polyg, const double &alpha) {
    PolygonByPoints res;
    for (auto point: polyg.points) {
        res.points.push_back(RotatePointOrigin(point, alpha));
    }
    return res;
}


PolygonByPoints MovePolygon(const PolygonByPoints &polyg, const Vector2d &shift) {
    PolygonByPoints res;
    for (auto point: polyg.points) {
        res.points.push_back(point + shift);
    }
    return res;
}

std::vector<PolygonByPoints> MoveMultiPolygon(const std::vector<PolygonByPoints> &polyg_list, const Vector2d &shift) {
    std::vector<PolygonByPoints> res;
    for (auto polyg: polyg_list) {
        res.push_back(MovePolygon(polyg, shift));
    }
    return res;
}

int FindMaxSegment(const PolygonByPoints &polyg) {
    int max_ind = -1;
    double max_len = -1;
    for (int i = 0; i < polyg.points.size(); ++i) {
        auto next_i = NextInd(i, polyg.points.size());
        auto segm_len = Dist2d(polyg.points[i], polyg.points[next_i]);
        if (segm_len > max_len) {
            max_len = segm_len;
            max_ind = i;
        }
    }
    return max_ind;
}

bool FindNearestIntersPoint(const Point2d &out_point, const Segment &segm,
        const std::vector<PolygonByPoints> &polyg_list, Point2d &nearest_point) {
    std::vector<Point2d> res_points;
    for (auto polyg: polyg_list) {
        PolygonBySegm pol_segm = ConvertPolygon(polyg);
        for (auto segm2: pol_segm.polygon) {
            if (Intersect(segm2, segm)) {
                std::vector<Point2d> res;
                LineIntersectionSpec(segm2, segm, res);
                res_points.insert(res_points.end(), res.begin(), res.end());
            }
        }
    }
    double min_dist = Infin;

    for (auto point: res_points) {
        if (Dist2d(out_point, point) < min_dist) {
            nearest_point = point;
            min_dist = Dist2d(out_point, point);
        }
    }
    return res_points.size() > 0;
}

Point2d FindDeepInner(const PolygonByPoints &pol) {
    PolygonByPoints new_pol;
    for (int i = 0; i < pol.points.size(); ++i) {
        auto point = pol.points[i];
        auto point_next = pol.points[(i + 1) % pol.points.size()];
        point = point + Vector2d(point, point_next) * 0.5;
        new_pol.points.push_back(point);
    }
    double sum_x = 0, sum_y = 0;
    for (auto point: new_pol.points) {
        sum_x += point.x;
        sum_y += point.y;
    }
    return Point2d(sum_x / pol.points.size(), sum_y / pol.points.size());
}

Point2d FindInner(const PolygonByPoints &pol) {
    Vector2d vect(pol.points[0], pol.points[1]);
    Vector2d ort = Norm(OrtVectOut(vect));
    // special case for metal frame office

    return Point2d(pol.points[0] + Vector2d(pol.points[0], pol.points[1]) * 0.5 + ort * 0.2);
}

double Perimeter(const PolygonByPoints &pol) {
    double res = 0;
    for (int i = 0; i < pol.points.size(); ++i) {
        res += Dist2d(pol.points[i], pol.points[(i + 1) % pol.points.size()]);
    }
    return res;
}


std::vector<Point2d> PolygCutIntersPoints(const Segment &segm1, const std::vector<PolygonByPoints> &polygons,
        std::vector<int> &polyg_ind_wo_doubles) {
    std::vector<Point2d> cut_points;
    std::vector<int> sorted_ind;
    std::vector<int> polygons_ind;

    int polyg_ind = -1, point_ind = -1;
    for (auto &polyg: polygons) {
        polyg_ind++;
        for (int i = 0; i < polyg.points.size(); ++i) {
            int i_next = (i + 1) % polyg.points.size();
            Point2d res;
            if (SegmentsIntersection(segm1.p1, segm1.p2, polyg.points[i], polyg.points[i_next], res)) {
                point_ind++;
                cut_points.push_back(res);
                polygons_ind.push_back(polyg_ind);
                sorted_ind.push_back(point_ind);
            }
        }
    }

    auto comp = [&](int a, int b) { return Dist2d(segm1.p1, cut_points[a]) < Dist2d(segm1.p1, cut_points[b]); };
    std::sort(sorted_ind.begin(), sorted_ind.end(), comp);
    std::vector<Point2d> cut_points_wo_doubles;

    if (!cut_points.size()) {
        return cut_points;
    }

    cut_points_wo_doubles.push_back(cut_points[sorted_ind[0]]);
    polyg_ind_wo_doubles.push_back(polygons_ind[sorted_ind[0]]);
    for (int j = 1; j < cut_points.size(); ++j) {
        if (!(cut_points[sorted_ind[j]] == cut_points[sorted_ind[j - 1]])) {
            cut_points_wo_doubles.push_back(cut_points[sorted_ind[j]]);
            polyg_ind_wo_doubles.push_back(polygons_ind[sorted_ind[j]]);
        }
    }
    return cut_points_wo_doubles;
}
