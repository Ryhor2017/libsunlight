//
// Created by ryhor on 05/11/18.
//

#ifndef STRUCTURE_FRAMES_GEN_OFFICE_CONVERTER_H
#define STRUCTURE_FRAMES_GEN_OFFICE_CONVERTER_H

#include "../settings.h"
#include "../input_output.h"
#include "./libsunlight.pb.h"


void Convert(const prosunlight::InInfo &proto, Input &input);

void Convert(const Output &output, prosunlight::OutInfo &proto);

#endif //STRUCTURE_FRAMES_GEN_OFFICE_CONVERTER_H
