//
// Created by ryhor on 05/11/18.
//
#include "converter.h"

void Convert(const prosunlight::InInfo &proto, Input &input) {
    int i = -1;
   // building
    for (auto contour: proto.polygon().contour_range()) {
        i++;
        PolygonByPoints polyg;
        std::vector<int> sect_cloused_segm(contour.point_range().size());
        auto proto_closed_segm = proto.closed_segments(i);
        int wall_ind = -1;
        for (auto point: contour.point_range()) {
            wall_ind++;
            polyg.points.push_back(Point2d(point.x(), point.y()));
            if (proto_closed_segm.range(wall_ind)) {
                sect_cloused_segm[wall_ind] = 1;
            }
        }
        if (!CheckClockwise(ConvertPolygon(polyg), 0.5)) {
            std::reverse(polyg.points.begin(), polyg.points.end());
        }
        input.closed_segments.push_back(sect_cloused_segm);
        input.buildings_src.push_back(polyg);
        input.buildings_heights_src.push_back(proto.h(i));
    }
   // neighbors building
    for (auto contour: proto.neighbors().contour_range()) {
        i++;
        PolygonByPoints polyg;
        for (auto point: contour.point_range()) {
            polyg.points.push_back(Point2d(point.x(), point.y()));
        }
        if (!CheckClockwise(ConvertPolygon(polyg), 0.5)) {
            std::reverse(polyg.points.begin(), polyg.points.end());
        }

        input.neighb_buildings_src.push_back(polyg);
        input.neighb_builds_heights_src.push_back(proto.neighbors_h(i));
    }
    input.latitude = proto.latitude();
    if (input.latitude < 0) {
        input.is_north = false;
    }
}

//bool NodeIsColumn(const Node &node) {
//    return node.type == NodeType::kGridNode || node.type == NodeType::kContourNode ||
//            node.type == NodeType::kColumn || node.type == NodeType::kGridContNode;
//}

void Convert(const Output &output, prosunlight::OutInfo &proto) {

    proto.set_integral_insol_sum(output.integral_insol_sum);
    proto.set_integral_insol_wint(output.integral_insol_wint);

    for (auto point_in: output.sunpath_sum) {
        prosunlight::Point *point = proto.add_sunpath_sum();
        point->set_x(point_in.x);
        point->set_y(point_in.y);
    }
    for (auto point_in: output.sunpath_wint) {
        prosunlight::Point *point = proto.add_sunpath_wint();
        point->set_x(point_in.x);
        point->set_y(point_in.y);
    }

    // default state
    proto.set_integral_insol(output.integral_insol);
    proto.set_best_integral_insol(output.best_integral_insol);
    proto.set_worst_integral_insol(output.worst_integral_insol);

    for (auto row: output.insolation_by_wall) {
        prosunlight::DoubleVec *row_proto = proto.add_insolation_by_wall();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto row: output.best_insolation) {
        prosunlight::DoubleVec *row_proto = proto.add_best_insolation();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto row: output.worst_insolation) {
        prosunlight::DoubleVec *row_proto = proto.add_worst_insolation();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto build: output.best_build) {
        prosunlight::Polygon *polyg = proto.mutable_best_build();
        for (auto point: build.points) {
            auto point_proto = *polyg->add_contour_range()->add_point_range();
            point_proto.set_x(point.x);
            point_proto.set_y(point.y);
        }
    }

    for (auto build: output.worst_build) {
        prosunlight::Polygon *polyg = proto.mutable_worst_build();
        for (auto point: build.points) {
            auto point_proto = *polyg->add_contour_range()->add_point_range();
            point_proto.set_x(point.x);
            point_proto.set_y(point.y);
        }
    }

    // summer state
    proto.set_integral_insol_sum(output.integral_insol_sum);
    proto.set_best_integral_insol_sum(output.best_integral_insol_sum);
    proto.set_worst_integral_insol_sum(output.worst_integral_insol_sum);

    for (auto row: output.insolation_by_wall_sum) {
        prosunlight::DoubleVec *row_proto = proto.add_insolation_by_wall_sum();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto row: output.best_insolation_sum) {
        prosunlight::DoubleVec *row_proto = proto.add_best_insolation_sum();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto row: output.worst_insolation_sum) {
        prosunlight::DoubleVec *row_proto = proto.add_worst_insolation_sum();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto build: output.best_build_sum) {
        prosunlight::Polygon *polyg = proto.mutable_best_build_sum();
        for (auto point: build.points) {
            auto point_proto = *polyg->add_contour_range()->add_point_range();
            point_proto.set_x(point.x);
            point_proto.set_y(point.y);
        }
    }

    for (auto build: output.worst_build_sum) {
        prosunlight::Polygon *polyg = proto.mutable_worst_build_sum();
        for (auto point: build.points) {
            auto point_proto = *polyg->add_contour_range()->add_point_range();
            point_proto.set_x(point.x);
            point_proto.set_y(point.y);
        }
    }

    // winter state
    proto.set_integral_insol_wint(output.integral_insol_wint);
    proto.set_best_integral_insol_wint(output.best_integral_insol_wint);
    proto.set_worst_integral_insol_wint(output.worst_integral_insol_wint);

    for (auto row: output.insolation_by_wall_wint) {
        prosunlight::DoubleVec *row_proto = proto.add_insolation_by_wall_wint();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto row: output.best_insolation_wint) {
        prosunlight::DoubleVec *row_proto = proto.add_best_insolation_wint();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto row: output.worst_insolation_wint) {
        prosunlight::DoubleVec *row_proto = proto.add_worst_insolation_wint();
        for (auto elem: row) {
            row_proto->add_range(elem);
        }
    }

    for (auto build: output.best_build_wint) {
        prosunlight::Polygon *polyg = proto.mutable_best_build_wint();
        for (auto point: build.points) {
            auto point_proto = *polyg->add_contour_range()->add_point_range();
            point_proto.set_x(point.x);
            point_proto.set_y(point.y);
        }
    }

    for (auto build: output.worst_build_wint) {
        prosunlight::Polygon *polyg = proto.mutable_worst_build_wint();
        for (auto point: build.points) {
            auto point_proto = *polyg->add_contour_range()->add_point_range();
            point_proto.set_x(point.x);
            point_proto.set_y(point.y);
        }
    }

    return;
}
