//
// Created by ryhor on 30/01/19.
//

#ifndef STRUCTURE_FRAMES_GEN_OFFICE_ALGEBRA_H
#define STRUCTURE_FRAMES_GEN_OFFICE_ALGEBRA_H

#include "headers.h"

std::vector<std::vector<int>> PermutationToCycles(std::vector<int> perm);

#endif //STRUCTURE_FRAMES_GEN_OFFICE_ALGEBRA_H
