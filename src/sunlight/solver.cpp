//
// Created by ryhor on 7/4/18.
//

#include "solver.h"
#include <assert.h>
#include <chrono>


using json = nlohmann::json;



//void InitSunPathDiagramV1(SunPathDiagramV1 &sun_path_diag) {
//// read sun_path json
//
//    std::string sun_path_file = "sunpath_diagram.json";
//    std::ifstream in;
//    in.open(sun_path_file);
//
//    json sun_path_json;
//    in >> sun_path_json;
//
//    float hour_detalization = static_cast<float>(sun_path_json["hour_detalization"].get<float>());
//    int max_latitude = static_cast<int>(sun_path_json["max_latitude"].get<int>());
//
//    sun_path_diag.Init(hour_detalization, max_latitude);
//
//    for (auto is_altitude: {true, false}) {
//        for (auto is_north: {true, false}) {
//            std::string json_table;
//            if (is_altitude) {
//                if (is_north) {
//                    json_table = "sin_altitude_n";
//                } else {
//                    json_table = "sin_altitude_s";
//                }
//            } else {
//                if (is_north) {
//                    json_table = "cos_azimuth_n";
//                } else {
//                    json_table = "cos_azimuth_s";
//                }
//            }
//            int latitude = -1;
//            for (auto &hour_days_val: sun_path_json[json_table]) {
//                latitude++;
//                int time_ind = -1;
//                for (auto &days_val: hour_days_val) {
//                    time_ind++;
//                    int day = -1;
//                    for (auto &val: days_val) {
//                        day++;
//                        float value = val.get<float>();
//                        sun_path_diag.SetCoordValue(latitude, time_ind, day, value, is_altitude, is_north);
//                    }
//                }
//            }
//        }
//    }
//    int time_ind = -1;
//    for (auto &time_days_val: sun_path_json["ksi_in_0_pi"]) {
//        time_ind++;
//        int day = -1;
//        for (auto &val: time_days_val) {
//            day++;
//            char value = val.get<int>();
//            sun_path_diag.SetKsiInValue(time_ind, day, value);
//        }
//    }
//
//    // test
////    SunCoord test = sun_path_diag.GetSunCoord(285, 12, 25, true);
////    auto angle = std::acos(test.azimuth_cos) *  180/3.14;
////    auto angle2 = std::asin(test.altitude_sin) *  180/3.14;
//
//    return;
//}

std::vector<Point2d> PolygCutIntersPointsSpecial(const int &omit_build_ind, const Segment &segm1,
                                                 const std::vector<PolygonByPoints> &polygons,
                                                 std::vector<int> &polyg_ind_wo_doubles) {
    std::vector<Point2d> cut_points;
    std::vector<int> sorted_ind;
    std::vector<int> polygons_ind;

    int polyg_ind = -1, point_ind = -1;
    for (auto &polyg: polygons) {
        polyg_ind++;
        if (polyg_ind == omit_build_ind) {
            continue;
        }
        for (int i = 0; i < polyg.points.size(); ++i) {
            int i_next = (i + 1) % polyg.points.size();
            Point2d res;
            if (SegmentsIntersection(segm1.p1, segm1.p2, polyg.points[i], polyg.points[i_next], res)) {
                point_ind++;
                cut_points.push_back(res);
                polygons_ind.push_back(polyg_ind);
                sorted_ind.push_back(point_ind);
            }
        }
    }

    auto comp = [&](int a, int b) { return Dist2d(segm1.p1, cut_points[a]) < Dist2d(segm1.p1, cut_points[b]); };
    std::sort(sorted_ind.begin(), sorted_ind.end(), comp);
    std::vector<Point2d> cut_points_wo_doubles;

    if (!cut_points.size()) {
        return cut_points;
    }

    cut_points_wo_doubles.push_back(cut_points[sorted_ind[0]]);
    polyg_ind_wo_doubles.push_back(polygons_ind[sorted_ind[0]]);
    for (int j = 1; j < cut_points.size(); ++j) {
        if (!(cut_points[sorted_ind[j]] == cut_points[sorted_ind[j - 1]])) {
            cut_points_wo_doubles.push_back(cut_points[sorted_ind[j]]);
            polyg_ind_wo_doubles.push_back(polygons_ind[sorted_ind[j]]);
        }
    }
    return cut_points_wo_doubles;
}

int RayIsPassing(const int &omit_build_ind, const int &segm_numb, const Point2d &point, const double &point_height,
             const double &tg_altitude,
             const Vector2d &dir_to_sun,
             const std::vector<PolygonByPoints> *building, const std::vector<double> *building_height,
             const std::vector<PolygonByPoints> *neighb_building, const std::vector<double> *neighb_builds_heights) {

    if (tg_altitude <= 0 || !building) {
        return 0;
    }
    auto vect_segm = Norm(Vector2d(building->at(omit_build_ind).points[segm_numb],
            building->at(omit_build_ind).points[(segm_numb + 1) % building->at(omit_build_ind).points.size()]));
    auto ort = OrtVectOut(vect_segm);
    if (ScalarProduct(ort, dir_to_sun) <= 0) {
        return 0;
    }
    Segment segm(point, point + dir_to_sun * 300); // normed dir_to_sun

        std::vector<Point2d> cut_points;
    std::vector<int> sorted_ind;
    std::vector<int> polygons_ind;

    const std::vector<double>*  all_heights;

    auto all_buildings = {building, neighb_building};

    int ind = -1;
    for (auto &building_s: all_buildings) {
        ind++;
        if (!building_s) {
            continue;
        }
        if (ind == 0) {
            all_heights = building_height;
        } else {
            all_heights = neighb_builds_heights;
        }
        int polyg_ind = -1, point_ind = -1;
        for (auto &polyg: *building_s) {
            polyg_ind++;
            for (int i = 0; i < polyg.points.size(); ++i) {
                int i_next = (i + 1) % polyg.points.size();
                Point2d inters_point;
                if (SegmentsIntersection(segm.p1, segm.p2, polyg.points[i], polyg.points[i_next], inters_point)) {
                    if (omit_build_ind == polyg_ind) {
                        if (Dist2d(inters_point, point) < 0.1) {
                            continue;
                        }
                    } else {
                        if (ind == 0) {
                            if (Dist2d(inters_point, point) < 0.1) {
                                return -1; // return closed windows_x
                            }
                        }
                    }
                    double height_b = all_heights->at(polyg_ind);
                    double delta = Dist2d(point, inters_point) * tg_altitude;
                    if (point_height + delta < height_b) {
                        return 0;
                    }
                }
            }
        }
    }

    return 1;
}



//double CalculateInsolationSRC(const Input &input) {
////  build_numbers - indexes bulduing from input.buildings;
//    int count_day = input.days_ind.size();
//    std::vector<std::vector<double>> res_aver_on_wall, res_aver_on_window; // average by day
//   // std::vector<std::vector<double>> insol_on_wall;
//    for (int ind = 0; ind < input.buildings->size(); ++ind) {
//        res_aver_on_wall.push_back(std::vector<double>(input.buildings->at(ind).points.size()));
//        res_aver_on_window.push_back(std::vector<double>(input.windows[ind].points.size()));
////        insol_on_wall.push_back(std::vector<double>(input.buildings->at(ind).points.size()));
//    }
//    std::vector<std::vector<double>> insol_on_windows = res_aver_on_window;
//    std::vector<double> insol_on_building(input.buildings->size());
//
//    for (int day_ind = 0; day_ind < input.days_ind.size(); ++day_ind) {
//        auto insol_on_windows_on_day = res_aver_on_window;
//        double count_sunny_hours = 0;
//        for (int hour = input.sun_path_diag.first_hour_day_ind[day_ind]; hour < input.sun_path_diag.last_hour_day_ind[day_ind] + 1; ++hour) {
//            auto sun_coord = input.sun_path_diag.sp_diag_local[day_ind][hour];
//            double tg_altitude =
//                    sun_coord.altitude_sin / std::sqrt(1 - sun_coord.altitude_sin * sun_coord.altitude_sin);
//            auto vect_to_sun = Vector2d(sun_coord.x, sun_coord.y);
//            if (sun_coord.altitude_sin < 0) {
//                continue;
//            }
//            count_sunny_hours++;
//            for (int build_ind = 0; build_ind < input.buildings->size(); ++build_ind) {
//                auto build_number = build_ind;
//                int wind_id = -1;
//                for (auto &point: input.windows[build_number].points) {
//                    wind_id++;
//                    int h_ind = -1;
//                    for (auto &h: input.windows_z[build_number]) {
//                        h_ind++;
//                        if (input.closed_windows.find(Grid3D(build_ind, wind_id, h_ind)) != input.closed_windows.end()) {
//                            continue;
//                        }
//                        auto res = static_cast<double>(RayIsPassing(build_number, input.walls_id[build_number][wind_id], point, h, tg_altitude, vect_to_sun,
//                                                                    input.buildings, input.buildings_heights,
//                                                                    input.neighb_buildings, input.neighb_builds_heights));
//                        if (res == -1) {
//                            res = 0;
////                            closed_windows.insert(Grid3D(build_ind, wind_id, h_ind));
////                            count_wind_on_wall[build_number][input.walls_id[build_number][wind_id]]--;
//                        }
//                        insol_on_windows_on_day[build_ind][wind_id] += res;
//                    }
//                }
//            }
//        }
//
//        for (int j = 0; j < insol_on_windows.size(); ++j) {
//            for (int t = 0; t < insol_on_windows[j].size(); ++t) {
//                if (count_sunny_hours) {
//                    insol_on_windows[j][t] += insol_on_windows_on_day[j][t] / count_sunny_hours;
//                }
//            }
//        }
//    }
//    double insolation = 0;
//    int total_windows_count = 0;
//    for (int build_ind = 0; build_ind < insol_on_windows.size(); ++build_ind) {
//        auto build_number = build_ind;
//        for (int t = 0; t < insol_on_windows[build_ind].size(); ++t) {
//            insol_on_windows[build_ind][t] /= count_day;
//            insol_on_building[build_ind] += insol_on_windows[build_ind][t];
//            insolation += insol_on_windows[build_ind][t];
//            auto wall_ind = input.walls_id[build_number][t];
////            if (insol_on_wall[build_ind][wall_ind] == -1) {
////                insol_on_wall[build_ind][wall_ind] = 0;
////            }
////            insol_on_wall[build_ind][wall_ind] += insol_on_windows[build_ind][t];
//        }
////        for (int i = 0; i < input.buildings->at(build_number).points.size(); ++i) {
////            if (input.count_wind_on_wall[build_number][i]) {
////                insol_on_wall[build_ind][i] /= input.count_wind_on_wall[build_number][i];
////            }
////        }
//        total_windows_count += input.count_wind_in_build[build_number];
//        insol_on_building[build_ind] /= input.windows[build_number].points.size();
//    }
//    insolation /= total_windows_count;
//
////    solution.insol_by_wall = insol_on_wall;
//
//    return insolation;
//}

double CalculateInsolation(const Input &input, Solution &solution) {
//  build_numbers - indexes bulduing from input.buildings;
    int count_day = input.days_ind.size();
    std::vector<std::vector<double>> res_aver_on_wall, res_aver_on_window; // average by day
    for (int ind = 0; ind < input.buildings->size(); ++ind) {
        res_aver_on_wall.push_back(std::vector<double>(input.buildings->at(ind).points.size()));
        res_aver_on_window.push_back(std::vector<double>(input.windows[ind].points.size()));
    }
    std::vector<std::vector<double>> insol_on_windows = res_aver_on_window;
    std::vector<std::vector<double>> insol_on_windows_summer = res_aver_on_window;
    std::vector<std::vector<double>> insol_on_windows_winter = res_aver_on_window;

    solution.insol_by_wall = res_aver_on_wall;
    solution.insol_by_wall_summer = res_aver_on_wall;
    solution.insol_by_wall_winter = res_aver_on_wall;

    std::vector<double> insol_on_building(input.buildings->size());

    for (int day_ind = 0; day_ind < input.days_ind.size(); ++day_ind) {
        auto insol_on_windows_on_day = res_aver_on_window;
        double count_sunny_hours = 0;
        for (int hour = input.sun_path_diag.first_hour_day_ind[day_ind]; hour < input.sun_path_diag.last_hour_day_ind[day_ind] + 1; ++hour) {
            auto sun_coord = input.sun_path_diag.sp_diag_local[day_ind][hour];
            double tg_altitude =
                    sun_coord.altitude_sin / std::sqrt(1 - sun_coord.altitude_sin * sun_coord.altitude_sin);
            auto vect_to_sun = Vector2d(sun_coord.x, sun_coord.y);
            if (sun_coord.altitude_sin < 0) {
                continue;
            }
            count_sunny_hours++;
            for (int build_ind = 0; build_ind < input.buildings->size(); ++build_ind) {
                auto build_number = build_ind;
                int wind_id = -1;
                for (auto &point: input.windows[build_number].points) {
                    wind_id++;
                    int h_ind = -1;
                    for (auto &h: input.windows_z[build_number]) {
                        h_ind++;
                        if (input.closed_windows.find(Grid3D(build_ind, wind_id, h_ind)) != input.closed_windows.end()) {
                            continue;
                        }
                        auto res = static_cast<double>(RayIsPassing(build_number, input.walls_id[build_number][wind_id], point, h, tg_altitude, vect_to_sun,
                                                                    input.buildings, input.buildings_heights,
                                                                    input.neighb_buildings, input.neighb_builds_heights));
                        if (res == -1) {
                            res = 0;
//                            closed_windows.insert(Grid3D(build_ind, wind_id, h_ind));
//                            count_wind_on_wall[build_number][input.walls_id[build_number][wind_id]]--;
                        }
                        insol_on_windows_on_day[build_ind][wind_id] += res;
                    }
                }
            }
        }

        for (int j = 0; j < insol_on_windows.size(); ++j) {
            for (int t = 0; t < insol_on_windows[j].size(); ++t) {
                if (count_sunny_hours) {
                    insol_on_windows[j][t] += insol_on_windows_on_day[j][t] / count_sunny_hours;
                    if (kSummerSolstice == input.days_ind[day_ind]) {
                        insol_on_windows_summer[j][t] = insol_on_windows_on_day[j][t] / count_sunny_hours;
                    }
                    if (kWinterSolstice == input.days_ind[day_ind]) {
                        insol_on_windows_winter[j][t] = insol_on_windows_on_day[j][t] / count_sunny_hours;
                    }
                }
            }
        }

    }
    solution.insolation = 0;
    solution.insol_summer = 0;
    solution.insol_winter = 0;
    int total_windows_count = 0;
    for (int build_ind = 0; build_ind < insol_on_windows.size(); ++build_ind) {
        auto build_number = build_ind;
        for (int wind_ind = 0; wind_ind < insol_on_windows[build_ind].size(); ++wind_ind) {
            insol_on_windows[build_ind][wind_ind] /= count_day;
            insol_on_building[build_ind] += insol_on_windows[build_ind][wind_ind];

            solution.insolation += insol_on_windows[build_ind][wind_ind];
            solution.insol_summer += insol_on_windows_summer[build_ind][wind_ind];
            solution.insol_winter += insol_on_windows_winter[build_ind][wind_ind];

            auto wall_ind = input.walls_id[build_number][wind_ind];
            if (solution.insol_by_wall[build_ind][wall_ind] == -1) {
                solution.insol_by_wall[build_ind][wall_ind] = 0;
            }
            solution.insol_by_wall[build_ind][wall_ind] += insol_on_windows[build_ind][wind_ind];

            if (solution.insol_by_wall_summer[build_ind][wall_ind] == -1) {
                solution.insol_by_wall_summer[build_ind][wall_ind] = 0;
            }
            solution.insol_by_wall_summer[build_ind][wall_ind] += insol_on_windows_summer[build_ind][wind_ind];

            if (solution.insol_by_wall_winter[build_ind][wall_ind] == -1) {
                solution.insol_by_wall_winter[build_ind][wall_ind] = 0;
            }
            solution.insol_by_wall_winter[build_ind][wall_ind] += insol_on_windows_winter[build_ind][wind_ind];
        }
        for (int i = 0; i < input.buildings->at(build_number).points.size(); ++i) {
            if (input.count_wind_on_wall[build_number][i]) {
                solution.insol_by_wall[build_ind][i] /= input.count_wind_on_wall[build_number][i];
                solution.insol_by_wall_summer[build_ind][i] /= input.count_wind_on_wall[build_number][i];
                solution.insol_by_wall_winter[build_ind][i] /= input.count_wind_on_wall[build_number][i];
            }
        }
        total_windows_count += input.count_wind_in_build[build_number];
        insol_on_building[build_ind] /= input.windows[build_number].points.size();
    }
    solution.insolation /= total_windows_count;
    solution.insol_summer /= total_windows_count;
    solution.insol_winter /= total_windows_count;

    return solution.insolation;
}

//void CalculateInsolationBySeasons(const std::vector<int> &build_numbers, const Input &input, Solution &solution) {
////    auto building = buildings[0];
//    int count_day = input.days_ind.size();
//    std::vector<double> res_aver_by_day(build_numbers.size()); // average by day
//    std::vector<std::vector<double>> res_aver_on_wall; // average by day
//    for (auto ind: build_numbers) {
//        res_aver_on_wall.push_back(std::vector<double>(input.buildings[ind].points.size()));
//    }
//    std::vector<std::vector<double>> insol_by_wall_winter_solstice = res_aver_on_wall,
//            insol_by_wall_summer_solstice = res_aver_on_wall;
//
//    std::vector<std::vector<double>> res_aver_by_day_on_wall = res_aver_on_wall; // average by day
////    std::vector<double> res_aver_by_day_on_wind(windows_x.points.size());
//
//    for (int day_ind = 0; day_ind < input.days_ind.size(); ++day_ind) {
//        std::vector<double> res_aver_by_hour(build_numbers.size());
//        std::vector<std::vector<double>> res_aver_by_hour_on_wall = res_aver_on_wall; // build_ind, wall_ind
//        double count_sunny_hours = 0;
//        for (int hour = 0; hour < 48; ++hour) {
//            auto sun_coord = input.sp_diag_local[day_ind][hour];
//            double tg_altitude =
//                    sun_coord.altitude_sin / std::sqrt(1 - sun_coord.altitude_sin * sun_coord.altitude_sin);
//            auto vect_to_sun = Vector2d(sun_coord.x, sun_coord.y);
//            if (sun_coord.altitude_sin < 0) {
//                continue;
//            }
//            count_sunny_hours++;
//            std::vector<std::vector<double>> res_aver_by_wind_on_wall = res_aver_on_wall;
//            for (int build_ind = 0; build_ind < build_numbers.size(); ++build_ind) {
//                auto build_number = build_numbers[build_ind];
//                int wind_id = -1;
//                for (auto &point: solution.windows_xy[build_number].points) {
//                    wind_id++;
//                    for (auto &h: input.windows_z[build_number]) {
//                        auto res = static_cast<double>(RayIsPassing(build_number, input.walls_id[build_number][wind_id],
//                                point, h, tg_altitude, vect_to_sun,
//                                                                    solution.buildings, input.buildings_heights,
//                                                                    input.neighb_buildings, input.neighb_builds_heights));
//                        res_aver_by_wind_on_wall[build_ind][input.walls_id[build_number][wind_id]] += res;
//                    }
//                }
//                for (int i = 0; i < res_aver_by_wind_on_wall[build_ind].size(); ++i) {
//                    int denom = input.count_wind_on_wall[build_number][i] * input.windows_z[build_number][i];
//                    if (denom == 0) {
//                        res_aver_by_hour_on_wall[build_ind][i] = -1;
//                    } else {
//                        res_aver_by_hour_on_wall[build_ind][i] += res_aver_by_wind_on_wall[build_ind][i] / denom;
//                        res_aver_by_hour[build_ind] += res_aver_by_hour_on_wall[build_ind][i];
//                    }
//                }
//            }
//        }
//        assert(count_sunny_hours > 0);
//        int build_ind = -1;
//        for (auto &res_aver_by_hour_i: res_aver_by_hour) {
//            build_ind++;
//            res_aver_by_hour_i /= count_sunny_hours;
//            res_aver_by_day[build_ind] += res_aver_by_hour_i;
//            for (int i = 0; i < res_aver_by_day_on_wall[build_ind].size(); ++i) {
//                res_aver_by_day_on_wall[build_ind][i] += res_aver_by_hour_on_wall[build_ind][i] / count_sunny_hours;
//                if (kSummerSolstice == input.days_ind[day_ind]) {
//                    insol_by_wall_summer_solstice[build_ind][i] =
//                            res_aver_by_hour_on_wall[build_ind][i] / count_sunny_hours;
//                }
//                if (kWinterSolstice == input.days_ind[day_ind]) {
//                    insol_by_wall_winter_solstice[build_ind][i] =
//                            res_aver_by_hour_on_wall[build_ind][i] / count_sunny_hours;
//                }
//            }
//
//            if (kSummerSolstice == input.days_ind[day_ind]) {
//                solution.insol_summer += res_aver_by_day[build_ind];
//            }
//            if (kWinterSolstice == input.days_ind[day_ind]) {
//                solution.insol_winter += res_aver_by_day[build_ind];
//            }
//        }
////            std::cout << "day: " << day << std::endl;
////            std::cout << "count_sunny_hours: " << count_sunny_hours << std::endl;
//    }
//    solution.insolation = 0;
//    for (int build_ind = 0; build_ind < build_numbers.size(); ++build_ind) {
//        res_aver_by_day[build_ind] /= count_day;
//        for (int i = 0; i < res_aver_by_day_on_wall.size(); ++i) {
//            if (res_aver_by_day_on_wall[build_ind][i] < 0) {
//                res_aver_by_day_on_wall[build_ind][i] = -1;
//            } else {
//                res_aver_by_day_on_wall[build_ind][i] /= count_day;
//            }
//        }
//        solution.insolation += res_aver_by_day[build_ind];
//    }
//    solution.insolation /= build_numbers.size();
//    solution.insol_summer /= build_numbers.size();
//    solution.insol_winter /= build_numbers.size();
//    solution.insol_by_wall = res_aver_by_day_on_wall;
//    solution.insol_by_wall_winter = insol_by_wall_winter_solstice;
//    solution.insol_by_wall_summer = insol_by_wall_summer_solstice;
//
//    return;
//}

void OptymizeIntegralInsolation(Input &input, std::vector<Solution> &solutions, std::vector<std::vector<double>> &insolation_by_angle_ls) {

    double best_res = -1, worst_res = Infin;
    double best_res_summ = -1, worst_res_summ = Infin;
    double best_res_wint = -1, worst_res_wint = Infin;
    std::vector<double> best_res_aver_by_day_on_wall;
    std::vector<double> best_res_by_wind;
    std::vector<double> res_aver_by_day_for_angles;
    std::vector<double> res_min_by_wind_for_angles;


    std::vector<int> angles(180);
    for (int j = 0; j < 180; ++j) {
        angles[j] = 2 * j;
    }

    // angles = {0};
    std::vector<double> min_wind_insol_by_angle(360);
    insolation_by_angle_ls.assign(3, std::vector<double>(360));

    auto start_solution = &solutions[0];
    auto best_solution = &solutions[1];
    auto worst_solution = &solutions[2];
    auto start_solution_summ = &solutions[3];
    auto best_solution_summ = &solutions[4];
    auto worst_solution_summ = &solutions[5];
    auto start_solution_wint = &solutions[6];
    auto best_solution_wint = &solutions[7];
    auto worst_solution_wint = &solutions[8];

    int ind = -1;
    for (int angle: angles) {
        ind++;
        SolutionParams params(angle);
        Solution solution;

        PrepareSolutionInput(params, input, solution);
        auto start_solver = std::chrono::high_resolution_clock::now();
        // ToDO
        //  CalculateInsolation(input.inds, input, solution);
        auto finish_solver = std::chrono::high_resolution_clock::now();
        CalculateInsolation(input, solution);

        std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
        std::cout << "init time: " << init_elapsed.count() << std::endl;

        insolation_by_angle_ls[0][2 * ind] = solution.insolation;
        insolation_by_angle_ls[1][2 * ind] = solution.insol_summer;
        insolation_by_angle_ls[2][2 * ind] = solution.insol_winter;
        if (ind) {
            for (int inn_ind: {0, 1, 2}) {
                insolation_by_angle_ls[inn_ind][2 * ind - 1] =
                        (insolation_by_angle_ls[inn_ind][2 * (ind - 1)] +
                         insolation_by_angle_ls[inn_ind][2 * ind]) / 2;
            }
            if (ind == angles.size() - 1) {
                for (int inn_ind: {0, 1, 2}) {
                    insolation_by_angle_ls[inn_ind][2 * ind + 1] =
                            (insolation_by_angle_ls[inn_ind][2 * ind] +
                             insolation_by_angle_ls[inn_ind][0]) / 2;
                }
            }
        }

        if (angle == 0) {
            *start_solution = solution;
            *start_solution_summ = solution;
            *start_solution_wint = solution;
        }
        if (solution.insolation > best_res) {
            *best_solution = solution;
            best_res = solution.insolation;
        }
        if (solution.insol_summer > best_res_summ) {
            *best_solution_summ = solution;
            best_res_summ = solution.insol_summer;
        }
        if (solution.insol_winter > best_res_wint) {
            *best_solution_wint = solution;
            best_res_wint = solution.insol_winter;
        }
        if (solution.insolation < worst_res) {
            *worst_solution = solution;
            worst_res = solution.insolation;
        }
        if (solution.insol_summer < worst_res_summ) {
            *worst_solution_summ = solution;
            worst_res_summ = solution.insol_summer;
        }
        if (solution.insol_winter < worst_res_wint) {
            *worst_solution_wint = solution;
            worst_res_wint = solution.insol_winter;
        }

//        std::cout << "angle: " << angle << ", res_aver_by_day: " << solution.insolation << ", best_res: " << best_res
//                  << std::endl;
        res_aver_by_day_for_angles.push_back(solution.insolation);

    }
}

//void CalculateInsolationByWindows(const std::vector<PolygonByPoints> &buildings, const PolygonByPoints &windows_x,
//                         const Input &input, const SolutionParams &params, Solution &solution) {
//    auto building = buildings[0];
//    int count_day = input.days_ind.size();
//    double res_aver_by_day = 0; // average by day
//    std::vector<double> res_aver_by_day_on_wall(building.points.size()); // average by day
//    std::vector<double> res_aver_by_day_on_wind(windows_x.points.size());
//
//    for (int day_ind = 0; day_ind < input.days_ind.size(); ++day_ind) {
//        double res_aver_by_hour = 0;
//        std::vector<double> res_aver_by_hour_on_wall(building.points.size());
//        std::vector<double> res_aver_by_hour_on_wind(windows_x.points.size());
//        double count_sunny_hours = 0;
//        for (int hour = 0; hour < 48; ++hour) {
//            auto sun_coord = input.sp_diag_local[day_ind][hour];
//            double tg_altitude =
//                    sun_coord.altitude_sin / std::sqrt(1 - sun_coord.altitude_sin * sun_coord.altitude_sin);
//            auto vect_to_sun = Vector2d(sun_coord.x, sun_coord.y);
//            if (sun_coord.altitude_sin < 0) {
//                continue;
//            }
//            count_sunny_hours++;
//            double res_aver_by_wind = 0;
//            std::vector<double> res_aver_by_wind_on_wall(building.points.size());
//            std::vector<int> count_wind_on_wall(building.points.size());
//            int wind_id = -1;
//            for (auto point: windows_x.points) {
//                wind_id++;
//                for (auto h: input.windows_z) {
//                    auto res = static_cast<double>(RayIsPassing(point, h, tg_altitude, vect_to_sun, buildings,
//                                                                input.neighb_builds_heights));
//                    res_aver_by_wind += res;
//                    res_aver_by_wind_on_wall[input.walls_id[wind_id]] += res;
//                    count_wind_on_wall[input.walls_id[wind_id]]++;
//                    res_aver_by_hour_on_wind[wind_id] += res;
//                }
//            }
//            for (int i = 0; i < res_aver_by_wind_on_wall.size(); ++i) {
//                int denom = count_wind_on_wall[i];
//                if (denom == 0) {
//                    res_aver_by_hour_on_wall[i] = -1;
//                } else {
//                    res_aver_by_hour_on_wall[i] += res_aver_by_wind_on_wall[i] / denom;
//                }
//            }
//            res_aver_by_wind /= input.windows_z.size() * windows_x.points.size();
//            res_aver_by_hour += res_aver_by_wind;
//        }
//        assert(count_sunny_hours > 0);
//        res_aver_by_hour /= count_sunny_hours;
//        res_aver_by_day += res_aver_by_hour;
//        for (int i = 0; i < res_aver_by_day_on_wall.size(); ++i) {
//            res_aver_by_day_on_wall[i] += res_aver_by_hour_on_wall[i] / count_sunny_hours;
//        }
//        for (int i = 0; i < res_aver_by_day_on_wind.size(); ++i) {
//            res_aver_by_day_on_wind[i] += res_aver_by_hour_on_wind[i] / count_sunny_hours;
//        }
////            std::cout << "day: " << day << std::endl;
////            std::cout << "count_sunny_hours: " << count_sunny_hours << std::endl;
//    }
//    res_aver_by_day /= count_day;
//    for (int i = 0; i < res_aver_by_day_on_wall.size(); ++i) {
//        if (res_aver_by_day_on_wall[i] < 0) {
//            res_aver_by_day_on_wall[i] = -1;
//        } else {
//            res_aver_by_day_on_wall[i] /= count_day;
//        }
//    }
//    double min_res_wind = Infin;
//    for (int j = 0; j < res_aver_by_day_on_wind.size(); ++j) {
//        res_aver_by_day_on_wind[j] /= count_day;
//        if (min_res_wind > res_aver_by_day_on_wind[j]) {
//            min_res_wind = res_aver_by_day_on_wind[j];
//        }
//    }
//    solution.insolation_by_wall = res_aver_by_day;
//    solution.insol_by_wall = res_aver_by_day_on_wall;
//    solution.insol_by_wind = res_aver_by_day_on_wind;
//    solution.min_res_wind = min_res_wind;
//    solution.params = params;
//
//    return;
//}

void PrepareSolutionInput(const SolutionParams &params, Input &input, Solution &solution) {
    int ind = -1;
    solution.params = params;
    for (auto &build: input.buildings_src) {
        ind++;
        solution.buildings.push_back(RotatePolygonOrigin(build, static_cast<double>(params.angle)));
//        solution.windows_xy.push_back(RotatePolygonOrigin(input.windows[ind], static_cast<double>(params.angle)));
    }
    input.set_buildings(solution.buildings, input.buildings_heights_src);
    return;
}

void CreateVisualJSONAllSolution(const std::vector<double> &insolation_by_angle,
                                 const std::vector<double> &min_wind_insol_by_angle, const std::string &file_name) {
    json root;
    root["insolation_by_angle"] = insolation_by_angle;
    root["min_wind_insol_by_angle"] = min_wind_insol_by_angle;
    std::ofstream file(file_name);
    file << root;
    file.close();
}

void TriangulateConvexPolygon(const PolygonByPoints &inp_polyg, PolygonByPoints &out_polyg) {
    for (int i = 0; i < inp_polyg.points.size(); ++i) {
        out_polyg.points.push_back(inp_polyg.points[i]);
        int i_next = (i + 1) % inp_polyg.points.size();
        auto p_cent = inp_polyg.points[i] + Vector2d(inp_polyg.points[i], inp_polyg.points[i_next]) * 0.5;
        out_polyg.points.push_back(p_cent);
    }
    auto inn_point = FindDeepInner(inp_polyg);
    int len = out_polyg.points.size();
    for (int j = 0; j < len; ++j) {
        out_polyg.points.push_back(out_polyg.points[j] + Vector2d(out_polyg.points[j], inn_point) * 0.5);
    }
    out_polyg.points.push_back(inn_point);
}

PolygonByPoints AddBorderPoints(const PolygonByPoints &polyg, const double &count) {
    PolygonByPoints res;
    for (int i = 0; i < polyg.points.size(); ++i) {
        res.points.push_back(polyg.points[i]);
        int i_next = (i + 1) % polyg.points.size();
        for (int j = 0; j < count - 1; ++j) {
            auto p_cent = polyg.points[i] + Vector2d(polyg.points[i], polyg.points[i_next]) * ((j + 1) / count);
            res.points.push_back(p_cent);
        }
    }
    return res;
}

//void CalculateSiteEnvelop(const VirtSunPathDiagram &sun_path_diagram, const Input &input, PolygonByPoints &site_grid,
//                          std::vector<double> &res) {
//    // NOTE: construction site = input.neighb_buildings[0]
//    int year_day = input.days_ind[0], latitude = input.latitude;
//    //site_triangle
//    std::vector<PolygonByPoints> buildings_add;
//    for (int j = 0; j < input.neighb_buildings.size(); ++j) {
//        buildings_add.push_back(AddBorderPoints(input.neighb_buildings[j], 20));
//    }
//    TriangulateConvexPolygon(AddBorderPoints(input.site, 20), site_grid);
//    res.assign(site_grid.points.size(), Infin);
//    for (int i = 0; i < site_grid.points.size(); ++i) {
//        auto point_site = site_grid.points[i];
//        for (int j = 0; j < input.neighb_buildings.size(); ++j) {
//            for (int k = 0; k < buildings_add[j].points.size(); ++k) {
//                auto point_n = buildings_add[j].points[k];
//                auto vect = Norm(Vector2d(point_n, point_site));
//                float azim_cos = vect.y;
//                InputSunCoord input_sun(year_day, 0, latitude, input.is_north);
//                auto sun_coord = sun_path_diagram.GetSunCoord(
//                        InputSunCoord(year_day, sun_path_diagram.GetHourIndex(input_sun, azim_cos,
//                                                                              sign(vect.x)), latitude, input.is_north));
//                double max_h = Infin;
//                if (std::fabs(sun_coord.altitude_sin - 1) > 1e-3 && sun_coord.altitude_sin > kMinAltitude) {
//                    max_h = Dist2d(point_n, point_site) * sun_coord.altitude_sin /
//                            std::sqrt(1 - sun_coord.altitude_sin * sun_coord.altitude_sin) +
//                            input.neighb_bottom_windows_heights[j];
//                }
//                res[i] = std::min(res[i], max_h);
//            }
//        }
//    }
//}

void CreateVisualJSON(const Input &input, const std::string &file_name) {
    Solution solution;
    solution.buildings = *input.buildings;
    solution.insolation = -1;
    solution.insol_by_wall = {};
    solution.windows_xy = input.windows;
    solution.params.angle = 0;
    CreateVisualJSON(solution, input, file_name);
}

void CreateVisualJSON(const Solution &solution, const Input &input, const std::string &file_name) {
        // save result to json for visualization

    json root, sun_path, buildings_json, neigh_buildings_json, insolation_by_wall;

    for (auto building: solution.buildings) {
        json building_json;
        for (auto point: building.points) {
            json pn;
            pn["x"] = point.x;
            pn["y"] = point.y;
            building_json.push_back(pn);
        }
        buildings_json.push_back(building_json);
    }
    root["buildings"] = buildings_json;
    if (input.neighb_buildings) {
        for (auto building: *input.neighb_buildings) {
            json building_json;
            for (auto point: building.points) {
                json pn;
                pn["x"] = point.x;
                pn["y"] = point.y;
                building_json.push_back(pn);
            }
            neigh_buildings_json.push_back(building_json);
        }
        root["neig_buildings"] = neigh_buildings_json;
    }

    if (solution.insol_by_wall.size()) {
        root["insolation_by_wall"] = solution.insol_by_wall;
    }

    for (auto days: input.sun_path_diag.sp_diag_local) {
        json days_js;
        for (auto sun_coord: days) {
            json coord;
            coord["x"] = sun_coord.x;
            coord["y"] = sun_coord.y;
            coord["alt_sin"] = sun_coord.altitude_sin;
            days_js.push_back(coord);
        }
        sun_path.push_back(days_js);
    }

    std::vector<std::vector<double>> windows_json;
    int build_ind = -1;
    for (auto wind_of_build: solution.windows_xy) {
        build_ind++;
        int wind_ind = -1;
        for (auto point: wind_of_build.points) {
            wind_ind++;
            bool wind_closed = true;
            for (int h = 0; h < input.windows_z[build_ind].size(); ++h) {
                if (input.closed_windows.find(Grid3D(build_ind, wind_ind, h)) == input.closed_windows.end()) {
                    wind_closed = false;
                    break;
                }
            }
            if (!wind_closed) {
                windows_json.push_back({point.x, point.y});
            }
        }
    }
    root["windows"] = windows_json;
    root["days"] = input.days_ind;

    root["sun_path_diag"] = sun_path;
    root["map_wind_to_wall"] = input.walls_id;

    root["latitude"] = input.latitude;
    if (input.neighb_builds_heights) {
        root["neib_build_height"] = *input.neighb_builds_heights;
    }
    root["build_height"] = *input.buildings_heights;
    root["longitude"] = 0;
    root["time_zone"] = 0;
    root["angle"] = solution.params.angle;
    root["integral_insolation"] = solution.insolation;

    std::ofstream file(file_name);

    file << root;
    file.close();
}

int ReadJSONInput(const std::string &file_name, Input &input, bool read_binary) {
    try {


        std::ifstream in;
        in.open(file_name);
        json input_json;
        in >> input_json;
        // test simple json parsing
        ju::parse_json(input_json, "closed_segments", input.closed_segments);

        // ju::parse_json(input_json, "win_heights", input.windows_z);

        std::vector<int> days_ind;
        ju::parse_json(input_json, "days", days_ind);

        // ToDo
        auto start_solver = std::chrono::high_resolution_clock::now();
        input.set_astronom_params(input_json["latitude"].get<int>(), days_ind, read_binary);
        auto finish_solver = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
        std::cout << "set_astronom_params time: " << init_elapsed.count() << std::endl;

        std::vector<double> buildings_heights;
        ju::parse_json(input_json, "buildings_heights", buildings_heights);

        std::vector<double> neighb_builds_heights;
        ju::parse_json(input_json, "neighb_builds_heights", neighb_builds_heights);

        std::vector<double> neighb_bottom_windows_heights;
        ju::parse_json(input_json, "neighb_bottom_windows_heights", neighb_bottom_windows_heights);

        for (auto &point: input_json["site"]) {
            input.site.points.push_back(Point2d(point.at(0).get<float>(), point.at(1).get<float>()));
        }

        std::vector<PolygonByPoints> buildings;
        int ind = -1;
        for (auto &build: input_json["buildings"]) {
            buildings.push_back(PolygonByPoints());
            ind++;
            for (auto &point: build) {
                buildings[ind].points.push_back(Point2d(point.at(0).get<float>(), point.at(1).get<float>()));
            }
        }
        input.set_buildings_src(buildings, buildings_heights);

        std::vector<PolygonByPoints> neighb_buildings;
        ind = -1;
        for (auto &build: input_json["neighb_buildings"]) {
            neighb_buildings.push_back(PolygonByPoints());
            ind++;
            for (auto &point: build) {
                neighb_buildings[ind].points.push_back(
                        Point2d(point.at(0).get<float>(), point.at(1).get<float>()));
            }
        }
        input.set_neighb_buildings_src(neighb_buildings, neighb_builds_heights);
    }
    catch (std::ifstream::failure &e) {
        std::cerr << "Exception open/reading file: " << e.what() << '\n';
        return 1;
    }
    return 0;
}

void Prepare3DVisualization(const Input &input, const SunPathDiagramV2 &sun_path_diag,
                            const PolygonByPoints &site_grid, const std::vector<double> &site_envelop,
                            const std::string output_dir) {

    int ind_b = -1;
    for (int i = 0; i < input.neighb_buildings->size(); ++i) {
        auto build = input.neighb_buildings[i];
        ind_b++;
        auto format_off = FormatOFF({build}, {input.neighb_builds_heights[ind_b]});
        format_off.CreateWalls(output_dir + "file_build" + std::to_string(ind_b) + ".off", {0, 0, 255});
    }

    // sunpath to 3D-visualization
    FormatOFF sunpath_off;
    double sphere_radius = 100;
    double cent_width = 2;
    sunpath_off.wall_points.push_back(Point3d(-cent_width, 0, 0));
    sunpath_off.wall_points.push_back(Point3d(0, cent_width, 0));
    sunpath_off.wall_points.push_back(Point3d(cent_width, 0, 0));
    sunpath_off.wall_points.push_back(Point3d(0, -cent_width, 0));
    sunpath_off.wall_points.push_back(Point3d(0, 0, cent_width));
    sunpath_off.wall_faces.push_back(Face({0, 1, 4}, {255, 0, 0}));
    sunpath_off.wall_faces.push_back(Face({1, 2, 4}, {255, 0, 0}));
    sunpath_off.wall_faces.push_back(Face({2, 3, 4}, {255, 0, 0}));
    sunpath_off.wall_faces.push_back(Face({3, 0, 4}, {255, 0, 0}));
    for (int hour = 0; hour < 24 * 2; ++hour) {
        auto coord = sun_path_diag.GetSunCoord(InputSunCoord(input.days_ind[0], hour, input.latitude, input.is_north));
        if (coord.altitude_sin < 0) {
            continue;
        }
        std::vector<double> colors = {255, 255, 0};
        if (coord.altitude_sin < kMinAltitude) {
            colors = {255, 0, 0};
        }
        double z = sphere_radius * coord.altitude_sin;
        double r = sphere_radius * std::sqrt(1 - coord.altitude_sin * coord.altitude_sin);
        int ind = sunpath_off.wall_points.size();
        sunpath_off.wall_points.push_back(Point3d(coord.x * r, coord.y * r, z));
        sunpath_off.wall_points.push_back(Point3d(coord.x * r * 0.90, coord.y * r * 0.90, z * 0.90));

        auto ort = Norm(OrtVectOut(Vector2d(coord.x * r, coord.y * r)));
        auto p_left = Point3d(coord.x * r, coord.y * r, z * 0.95) + Convert3D(ort, 0);
        auto p_right = Point3d(coord.x * r, coord.y * r, z * 0.95) + Convert3D(ort, 0) * (-1);
        sunpath_off.wall_points.push_back(p_left);
        sunpath_off.wall_points.push_back(p_right);

        sunpath_off.wall_faces.push_back(Face({ind + 2, ind + 0, ind + 1}, colors));
        sunpath_off.wall_faces.push_back(Face({ind + 3, ind + 2, ind + 0}, colors));
        sunpath_off.wall_faces.push_back(Face({ind + 3, ind + 1, ind + 0}, colors));
        sunpath_off.wall_faces.push_back(Face({ind + 2, ind + 3, ind + 1}, colors));
    }
    sunpath_off.CreateOFFfile(output_dir + "sun_path.off");
    auto format_off = FormatOFF({site_grid}, {site_envelop});
    format_off.CreateEnvelop((site_grid.points.size() - 1) / 2, site_grid, site_envelop,
                             output_dir + "file_envelop.off");
    return;
}

void PrepareSimpleDashboard(const Input &input, const std::vector<Solution> &solutions,
        const std::vector<std::vector<double>> insolation_by_angle_ls, Output &output) {

    auto start_solution = solutions[0];
    auto best_solution = solutions[1];
    auto worst_solution = solutions[2];
    auto start_solution_summ = solutions[3];
    auto best_solution_summ = solutions[4];
    auto worst_solution_summ = solutions[5];
    auto start_solution_wint = solutions[6];
    auto best_solution_wint = solutions[7];
    auto worst_solution_wint = solutions[8];

    for (int hour = 0; hour < input.sun_path_diag.last_hour_ind - input.sun_path_diag.first_hour_ind + 1; ++hour) {
        auto coord_wint = input.sun_path_diag.GetSunCoord(
                InputSunCoord(kWinterSolsticeInd, hour, input.latitude, input.is_north));
        auto coord_summ = input.sun_path_diag.GetSunCoord(
                InputSunCoord(kSummerSolsticeInd, hour, input.latitude, input.is_north));
        if (coord_summ.altitude_sin > kMinAltitudeToStartCalcul) {
            output.sunpath_sum.push_back(Point2d(coord_summ.x, coord_summ.y));
        }
        if (coord_wint.altitude_sin > kMinAltitudeToStartCalcul) {
            output.sunpath_wint.push_back(Point2d(coord_wint.x, coord_wint.y));
        }
    }
    // default state
    output.integral_insol = start_solution.insolation;
    output.best_integral_insol = best_solution.insolation;
    output.worst_integral_insol = worst_solution.insolation;

    output.insolation_by_wall = start_solution.insol_by_wall;
    output.best_insolation = best_solution.insol_by_wall;
    output.worst_insolation = worst_solution.insol_by_wall;

    output.best_build = best_solution.buildings;
    output.worst_build = worst_solution.buildings;

    output.insolation_by_angle = insolation_by_angle_ls[0];

    // summer state
    output.integral_insol_sum = start_solution.insol_summer;
    output.best_integral_insol_sum = best_solution.insol_summer;
    output.worst_integral_insol_sum = worst_solution.insol_summer;

    output.insolation_by_wall_sum = start_solution.insol_by_wall_summer;
    output.best_insolation_sum = best_solution.insol_by_wall_summer;
    output.worst_insolation_sum = worst_solution.insol_by_wall_summer;

    output.best_build_sum = best_solution_summ.buildings;
    output.worst_build_sum = worst_solution_summ.buildings;

    output.insolation_by_angle_sum = insolation_by_angle_ls[1];

    // winter state
    output.integral_insol_wint = start_solution.insol_winter;
    output.best_integral_insol_wint = best_solution.insol_winter;
    output.worst_integral_insol_wint = worst_solution.insol_winter;

    output.insolation_by_wall_wint = start_solution.insol_by_wall_winter;
    output.best_insolation_wint = best_solution.insol_by_wall_winter;
    output.worst_insolation_wint = worst_solution.insol_by_wall_winter;

    output.best_build_wint = best_solution_summ.buildings;
    output.worst_build_wint = worst_solution_summ.buildings;

    output.insolation_by_angle_win = insolation_by_angle_ls[2];
}

void Solver(Input &input, Output &output, bool prepare_json_visual) {
    // solver v.1 return sunpathdiagram (4 days), the best/worst solutions
    input.set_astronom_params(input.latitude, kPrecalculatedDays);
    input.set_neighb_buildings(input.neighb_buildings_src, input.neighb_builds_heights_src);

    std::vector<Solution> solutions(9);
    std::vector<std::vector<double>> insolation_by_angle_ls;
    OptymizeIntegralInsolation(input, solutions, insolation_by_angle_ls);
    PrepareSimpleDashboard(input, solutions, insolation_by_angle_ls, output);

    if (prepare_json_visual) {
        CreateVisualJSON(solutions[0], input, "output_test.json");
        CreateVisualJSON(solutions[1], input, "output_test_best.json");
        CreateVisualJSON(solutions[2], input, "output_test_worst.json");
        CreateVisualJSONAllSolution(insolation_by_angle_ls[0], {}, "output_test_allres.json");
    }
}
