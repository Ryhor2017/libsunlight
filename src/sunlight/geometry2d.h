//
// Created by ryhor on 01/08/18.
//

#ifndef PACKING2D_GEOMETRY_H
#define PACKING2D_GEOMETRY_H

#include <cmath>
#include <math.h>

#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>

const double EPS = 1E-9; // for compare double
const double EPSSoft = 1E-3; // for compare geom object equality
const double kEpsForPoint2d = 0.1; // for compare Point2d
const double Infin = 1e9;

namespace dbl {

    bool Less(double x, double y);

    bool Less(double x, double y, double eps);

    bool Equal(double x, double y);

    bool Equal(double x, double y, double eps);

    bool EqualLess(double x, double y, double eps);
}

int RoundDouble(double x);

struct Point2d {
    Point2d() {};

    Point2d(double x_, double y_) {
        x = x_;
        y = y_;
    }

    double x, y;
};

struct Segment {
    Point2d p1;
    Point2d p2;

    Segment() {};

    Segment(Point2d p1_, Point2d p2_) {
        p1 = p1_;
        p2 = p2_;
    }
};

struct Vector2d {
    Vector2d(): x(0), y(0) {};

    Vector2d(double x_, double y_) {
        x = x_;
        y = y_;
    }

    Vector2d(Point2d p1, Point2d p2) {
        x = p2.x - p1.x;
        y = p2.y - p1.y;
    }

    Vector2d(Point2d p1) {
        x = p1.x;
        y = p1.y;
    }

    Vector2d(const Segment &seg1) {
        x = seg1.p2.x - seg1.p1.x;
        y = seg1.p2.y - seg1.p1.y;
    }

    double x, y;
};


bool operator == (Point2d p1, Point2d p2);

int NextInd(const int &ind, const int &size);

// vector space operations
Vector2d operator * (Vector2d vect, double coef);

Point2d operator + (Point2d point, Vector2d vect);

Vector2d operator + (Vector2d vect1, Vector2d vect2);

struct PolygonByPoints {
    std::vector<Point2d> points;
};

struct PolygonBySegm {
    std::vector<Segment> polygon;
};

bool PointInPolygon(const Point2d &point, const PolygonBySegm &pol);

bool PointInPolygon(const Point2d &point, const PolygonByPoints &pol);

bool SmallRectOutPolygon(const PolygonByPoints &rect, const PolygonBySegm &pol);

PolygonBySegm ConvertPolygon(PolygonByPoints pol);

PolygonByPoints ConvertPolygon(PolygonBySegm pol);

PolygonBySegm CreateEnvelop(PolygonBySegm &pol, double shift);

bool CheckClockwise(PolygonBySegm pol, const double &shift);

bool CheckInnerCornerGeo(Point2d p1, Point2d p2, Point2d p3);

bool CheckOuterCornerGeo(Point2d p1, Point2d p2, Point2d p3);

double Dist2d(const Point2d &p1, const Point2d &p2);

double Dist2d(Segment segm);

double OrientAngle(Vector2d vect1, Vector2d vect2);

Vector2d Vect(Segment seg);

Vector2d Inverse(Vector2d vec);

Vector2d Norm(const Vector2d &vec);

Vector2d OrtVectOut(const Vector2d &vec);

bool Intersect(const Point2d &a, const Point2d &b, const Point2d &c, const Point2d &d);

bool LineIntersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4, Point2d &res);

PolygonByPoints CreateEnvelop(PolygonByPoints pol, std::vector<double> shifts);

double IntersectionRectPolygArea(const PolygonByPoints &rect, const PolygonByPoints &pol2);

bool PolygonsIntersect(const PolygonByPoints &pol1, const PolygonByPoints &pol2);

double IntersectionRectPolygArea(const PolygonByPoints &rect, const PolygonByPoints &pol2);

bool CoveredBy(const PolygonByPoints &pol_in, const PolygonByPoints &pol_out);

bool VectAntiCollinear(Vector2d vect2D_1, Vector2d vect2D_2);

int sign(double x);

bool PolygonsIntersectSpecial(const PolygonByPoints &pol1, const PolygonByPoints &pol2);

bool Intersect(const Segment &segm1, const Segment &segm2);

bool VectCollinear(Vector2d vect2D_1, Vector2d vect2D_2);

bool LineIntersectionSpec(const Segment &segm1, const Segment &segm2, std::vector<Point2d> &res);

Point2d RotatePointOrigin(Point2d point, double alpha);

PolygonByPoints RotatePolygonOrigin(const PolygonByPoints &polyg, const double &alpha);

int FindMaxSegment(const PolygonByPoints &polyg);

PolygonByPoints MovePolygon(const PolygonByPoints &polyg, const Vector2d &shift);

std::vector<PolygonByPoints> MoveMultiPolygon(const std::vector<PolygonByPoints> &polyg_list, const Vector2d &shift);

bool FindNearestIntersPoint(const Point2d &out_point, const Segment &segm,
        const std::vector<PolygonByPoints> &polyg_list, Point2d &nearest_point);

bool IntersectSegmPolyg(const Segment &segm1, const PolygonByPoints &polyg);

Point2d FindDeepInner(const PolygonByPoints &pol);

Point2d FindInner(const PolygonByPoints &pol);

void CreateEnvelop(PolygonByPoints &pol, double shift);

double ScalarProduct(Vector2d v1, Vector2d v2);
//PolygonByPoints CreateEnvelop(PolygonByPoints pol, double shifts);

double Perimeter(const PolygonByPoints &pol);


bool SegmentsIntersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4, Point2d &res);

std::vector<Point2d> PolygCutIntersPoints(const Segment &segm1, const std::vector<PolygonByPoints> &polygons,
        std::vector<int> &polygons_ind);

bool PointInPolygon(const Point2d &point, const std::vector<PolygonByPoints> &polygons);

bool PointOnSegment(const Point2d &point, const Segment &segm, const double &eps);

bool Equal(const Point2d &p1, const Point2d &p2, const double & eps);

bool PointOnContour(const Point2d &point, const PolygonByPoints &pol);

bool Intersect(const Segment &segm1, const Segment &segm2, const double &eps);

#endif //PACKING2D_GEOMETRY_H
