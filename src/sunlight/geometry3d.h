//
// Created by ryhor on 28/08/18.
//

#ifndef BIDIRECTIONAL_LIST_TEST_GEOMETRY3D_H
#define BIDIRECTIONAL_LIST_TEST_GEOMETRY3D_H


#include "./geometry2d.h"

struct Point3d {
    Point3d() {};

    Point3d(double x_, double y_, double z_) {
        x = x_;
        y = y_;
        z = z_;
    }

    double x, y, z;
};

struct Vector3d {
    Vector3d() {};

    Vector3d(double x_, double y_, double z_) {
        x = x_;
        y = y_;
        z = z_;
    }

    double x, y, z;
};

struct Segment3d {
    Point3d p1;
    Point3d p2;

    Segment3d() {};

    Segment3d(Point3d p1_, Point3d p2_) {
        p1 = p1_;
        p2 = p2_;
    }
};

struct PolygonByPoints3d {
    std::vector<Point3d> points;
};

bool operator == (Point3d p1, Point3d p2);


// vector space operations
Vector3d operator * (Vector3d vect, double coef);

Point3d operator + (Point3d point, Vector3d vect);

Point3d Convert3D(Point2d vect);

Point3d Convert3D(Point2d vect, double z);

Vector3d Convert3D(Vector2d vect, double z);

#endif //BIDIRECTIONAL_LIST_TEST_GEOMETRY3D_H
