#pragma once

#include <string>
#include <vector>

double const temp_x = 100, temp_y = 150;
const float kInf = 1e9;
const double kEps = 0.01;
const double kMinAltitude = 0.30; // for calculate site envelop/ 0.15 = 10 градусов
const double kMinHeight = 0; //
const std::vector<int> kPrecalculatedDays = {172, 356, 79, 266};
const int kSummerSolstice = 172; //172;
const int kSummerSolsticeInd = 0; //172;
const int kWinterSolstice = 356; //356;
const int kWinterSolsticeInd = 1; //356;
const std::string kOutputDir = "../test_output/";

const double kMinAltitudeToStartCalcul = 0.05; // use to ommit first and last hour
