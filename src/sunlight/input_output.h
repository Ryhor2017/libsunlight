//
// Created by ryhor on 02/11/18.
//

#ifndef STRUCTURE_FRAMES_GEN_OFFICE_INOUTOUTPUT_H
#define STRUCTURE_FRAMES_GEN_OFFICE_INOUTOUTPUT_H

#include "headers.h"
#include "geometry2d.h"
#include "celestial_coordinate_system.h"
#include <set>

struct Grid3D {
    int x, y, z;
    Grid3D(const int &x_, const int &y_, const int &z_) {
        x = x_;
        y = y_;
        z = z_;
    }
};

bool operator<(const Grid3D &cl, const Grid3D &cr);

class Input {
public:
    Input(){};

    // information about closed walls
    std::vector<std::vector<int>> closed_segments;

    // 3. calculated parameters
    std::vector<PolygonByPoints> windows;
    std::vector<std::vector<int>> walls_id;
    std::vector<std::vector<int>> count_wind_on_wall;
    std::vector<int> count_wind_in_build;

    std::vector<int> inds;
    PolygonByPoints site;


    void InitAstronomical(const std::vector<int> &year_days, const int &latitude) {
            InitSunPathDiagram(year_days, latitude);
    }

    void InitAstronomical( const int &latitude_) {
            InitSunPathDiagramBinary(latitude_);
    }

    void set_astronom_params(const float &latitude_, const std::vector<int> &days_ind_ = kPrecalculatedDays, const bool &read_binary = false) {
        latitude = latitude_;
        days_ind = days_ind_;
        if (read_binary) {
            InitAstronomical(latitude);
        } else {
            InitAstronomical(days_ind, latitude);
        }
        return;
    }
    void set_buildings(const std::vector<PolygonByPoints> &buildings_, const std::vector<double> &buildings_heights_) {
        buildings = &buildings_;
        buildings_heights = &buildings_heights_;
        if (windows_z.empty()) {
            windows_z.assign(buildings->size(), {3});
        }
        CalculateWindows();
    }
    void set_neighb_buildings(const std::vector<PolygonByPoints> &neighb_buildings_, const std::vector<double> &neighb_buildings_heights_) {
        neighb_buildings = &neighb_buildings_;
        neighb_builds_heights = &neighb_buildings_heights_;
    }

    void set_buildings_src(std::vector<PolygonByPoints> buildings_, std::vector<double> buildings_heights_) {
        buildings_src = buildings_;
        buildings_heights_src = buildings_heights_;
        if (windows_z.empty()) {
            windows_z.assign(buildings_.size(), {3});
        }
    }

    void set_neighb_buildings_src(std::vector<PolygonByPoints> &neighb_buildings_, std::vector<double> &neighb_buildings_heights_) {
        neighb_buildings_src = neighb_buildings_;
        neighb_builds_heights_src = neighb_buildings_heights_;
    }

    bool is_north = true;
    SunPathDiagramV2 sun_path_diag;
    std::set<Grid3D> closed_windows;

    // 1. required parameters
    // astronomical
    float latitude;
	std::vector<int> days_ind;
    // our buildings
    const std::vector<PolygonByPoints> *buildings = nullptr;
    const std::vector<double> *buildings_heights = nullptr;

    // 2. optional parameters
    // windows
    std::vector<std::vector<double>> windows_z;

    // neighb buildings
    const std::vector<PolygonByPoints> *neighb_buildings = nullptr;
    const std::vector<double> *neighb_builds_heights = nullptr;
    const std::vector<double> *neighb_bottom_windows_heights = nullptr; // need to calculate envelop

    // 3. service parameters
    std::vector<PolygonByPoints> buildings_src;
    std::vector<double> buildings_heights_src;
    std::vector<PolygonByPoints> neighb_buildings_src;
    std::vector<double> neighb_builds_heights_src;

    double step = 6; //distance between windows, meter
private:

    void CalculateWindows() {
        // calculate windows points
        windows.clear();
        int build_ind = -1;
        count_wind_in_build.assign((*buildings).size(), 0);
        for (auto &section: *buildings) {
            build_ind++;
            windows.push_back({});
            walls_id.push_back({});
            inds.push_back(build_ind);
            std::vector<int> tmp_vect(section.points.size());
            for (int k = 0; k < section.points.size(); ++k) {
                if (closed_segments.size() && closed_segments[build_ind][k]) {
                    continue;
                }
                auto p1 = section.points[k];
                auto p2 = section.points[(k + 1) % section.points.size()];
                double dist = Dist2d(p1, p2);
                int wind_count = static_cast<int>((dist - step) / step) + 1;
                auto vect = Norm(Vector2d(p1, p2));
                double step_local = step;
                if (wind_count <= 0) {
                    step_local = 0;
                    wind_count = 1;
                }
                count_wind_in_build[build_ind] += wind_count;
                double shift = (dist - (wind_count - 1) * step) / 2;
                int wind_count_adj = 0;
                for (int i = 0; i < wind_count; ++i) {
                    auto point = p1 + vect * (shift + step_local * i);
                    int build_neigb_ind, point_neighb_ind;
                    auto wind_closed = CheckClosedWindows(build_ind, point, Segment(p1, p2), build_neigb_ind, point_neighb_ind);
                    int wind_neihb_count_adj = 0, wall_neighb_ind = 0;
                    if (wind_closed) {
                        wall_neighb_ind = walls_id[build_neigb_ind][point_neighb_ind];
                        if (buildings_heights->at(build_neigb_ind) >= windows_z[build_ind][windows_z[build_ind].size() - 1]) {
                            for (int h_ind = 0; h_ind < windows_z[build_neigb_ind].size(); ++h_ind) {
                                if (buildings_heights->at(build_ind) >= windows_z[build_neigb_ind][h_ind]) {
                                    closed_windows.insert(Grid3D(build_neigb_ind, point_neighb_ind, h_ind));
                                    wind_neihb_count_adj++;
                                }
                            }
                            wind_count_adj += windows_z[build_ind].size();
                            count_wind_on_wall[build_neigb_ind][wall_neighb_ind] -= wind_neihb_count_adj;
                            count_wind_in_build[build_neigb_ind] -= wind_neihb_count_adj;
                            continue;
                        } else {
                            windows[windows.size() - 1].points.push_back(point);
                            walls_id[walls_id.size() - 1].push_back(k);

                            for (int h_ind = 0; h_ind < windows_z[build_ind].size(); ++h_ind) {
                                if (buildings_heights->at(build_neigb_ind) >= windows_z[build_ind][h_ind]) {
                                    closed_windows.insert(Grid3D(build_ind, windows[windows.size() - 1].points.size() - 1, h_ind));
                                    wind_count_adj++;
                                }
                            }
                            for (int h_ind = 0; h_ind < windows_z[build_neigb_ind].size(); ++h_ind) {
                                closed_windows.insert(Grid3D(build_neigb_ind, point_neighb_ind, h_ind));
                                wind_neihb_count_adj++;
                            }
                            count_wind_on_wall[build_neigb_ind][wall_neighb_ind] -= wind_neihb_count_adj;
                            count_wind_in_build[build_neigb_ind] -= wind_neihb_count_adj;
                        }
                    }
                    windows[windows.size() - 1].points.push_back(point);
                    walls_id[walls_id.size() - 1].push_back(k);
                }
                tmp_vect[k] = wind_count * windows_z[build_ind].size() - wind_count_adj;
                count_wind_in_build[build_ind] -= wind_count_adj;
            }
            count_wind_on_wall.push_back(tmp_vect);
        }
//        all_buildings = buildings;
//        all_buildings.insert(all_buildings.end(), neighb_buildings.begin(), neighb_buildings.end());
        return;
    };

    void InitSunPathDiagram(const std::vector<int> &year_days, const int &latitude_) {
    // calculate sun_path diagram
        sun_path_diag.latitude = latitude_;
        sun_path_diag.is_north = latitude >= 0;
        std::vector<int> new_days_ind(year_days.size());
        for (int i = 0; i < new_days_ind.size(); ++i) {
            new_days_ind[i] = i;
        }
        sun_path_diag.days_ind = new_days_ind;

        sun_path_diag.Init(year_days, latitude);

        sun_path_diag.CalculateSunCoord();

        return;
    }

    void InitSunPathDiagramBinary( const int &latitude_, const std::string up_path = "../../") {
    // read sun_path binary
        latitude = latitude_;
        is_north = latitude >= 0;
        sun_path_diag.latitude = latitude_;
        sun_path_diag.is_north = is_north;

        std::string is_north_ch = "n";
        if (!is_north) {
            is_north_ch = "s";
        }

        std::vector<int> new_days_ind(kPrecalculatedDays.size());
        for (int i = 0; i < new_days_ind.size(); ++i) {
            new_days_ind[i] = i;
        }
        days_ind = new_days_ind;
        sun_path_diag.days_ind = days_ind;
        std::string sun_path_file_azimalt =
                up_path + "src/sunlight/precalculated_sunpathdiag/azim_altit_" + is_north_ch + "_" + std::to_string(
                        static_cast<int>(latitude));
        std::string sun_path_file_ksi = up_path + "src/sunlight/precalculated_sunpathdiag/ksi_maxlat_hourdetal";

        sun_path_diag.Init(sun_path_file_azimalt, sun_path_file_ksi);

        sun_path_diag.CalculateSunCoord();

        return;
    }



    bool CheckClosedWindows(const int &build_ind, const Point2d &point, const Segment &segm, int &build_neigb_ind, int &point_neighb_ind) {                    //
        bool wind_closed = false;
        for (build_neigb_ind = 0; build_neigb_ind < windows.size(); ++build_neigb_ind) {
            if (build_neigb_ind == build_ind) {
                continue;
            }
            for (point_neighb_ind = 0; point_neighb_ind < windows[build_neigb_ind].points.size(); ++point_neighb_ind) {
                auto point_neigb = windows[build_neigb_ind].points[point_neighb_ind];
                auto point_neigb_next = windows[build_neigb_ind].points[(point_neighb_ind + 1) %
                                                                        windows[build_neigb_ind].points.size()];
                if (Dist2d(point_neigb, point) < kEpsForPoint2d) {
                    wind_closed = true;
                    break;
                } else {
                    if (Dist2d(point_neigb, point) < 2 * step) {
                        if (PointOnSegment(point, Segment(point_neigb, point_neigb_next), kEpsForPoint2d) &&
                            PointOnSegment(point_neigb, segm, kEpsForPoint2d)) {
                            wind_closed = true;
                            break;
                        }
                    }
                }
            }
            if (wind_closed) {
                break;
            }
        }
        return wind_closed;
    }
};


struct Output {
    // polar diagram
    std::vector<Point2d> sunpath_sum;
    std::vector<Point2d> sunpath_wint;

    // default state
    double integral_insol;
    double best_integral_insol;
    double worst_integral_insol;

    std::vector<std::vector<double>> insolation_by_wall;
    std::vector<std::vector<double>> best_insolation;
    std::vector<std::vector<double>> worst_insolation;

    std::vector<PolygonByPoints> best_build;
    std::vector<PolygonByPoints> worst_build;

    std::vector<double> insolation_by_angle;

    // summer state
    double integral_insol_sum;
    double best_integral_insol_sum;
    double worst_integral_insol_sum;

    std::vector<PolygonByPoints> best_build_sum;
    std::vector<PolygonByPoints> worst_build_sum;

    std::vector<std::vector<double>> insolation_by_wall_sum;
    std::vector<std::vector<double>> best_insolation_sum;
    std::vector<std::vector<double>> worst_insolation_sum;

    std::vector<double> insolation_by_angle_sum;

    // winter state
    double integral_insol_wint;
    double best_integral_insol_wint;
    double worst_integral_insol_wint;

    std::vector<PolygonByPoints> best_build_wint;
    std::vector<PolygonByPoints> worst_build_wint;

    std::vector<std::vector<double>> insolation_by_wall_wint;
    std::vector<std::vector<double>> best_insolation_wint;
    std::vector<std::vector<double>> worst_insolation_wint;

    std::vector<double> insolation_by_angle_win;
};

#endif //STRUCTURE_FRAMES_GEN_OFFICE_INOUTOUTPUT_H
