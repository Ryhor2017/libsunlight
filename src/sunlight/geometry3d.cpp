//
// Created by ryhor on 28/08/18.
//

#include "./geometry3d.h"
#include "./geometry2d.h"

bool operator == (Point3d p1, Point3d p2) {
    return dbl::Equal(p1.x, p2.x, kEpsForPoint2d) && dbl::Equal(p1.y, p2.y, kEpsForPoint2d) && dbl::Equal(p1.z, p2.z, kEpsForPoint2d);
}


// vector space operations
Vector3d operator * (Vector3d vect, double coef) {
    return Vector3d(vect.x * coef, vect.y * coef, vect.z * coef);
}

Point3d operator + (Point3d point, Vector3d vect) {
    return Point3d(point.x + vect.x, point.y + vect.y, point.z + vect.z);
}

Point3d Convert3D(Point2d vect) {
    return Point3d(vect.x, vect.y, 0);
}

Point3d Convert3D(Point2d vect, double z) {
    return Point3d(vect.x, vect.y, z);
}

Vector3d Convert3D(Vector2d vect, double z) {
    return Vector3d(vect.x, vect.y, z);
}