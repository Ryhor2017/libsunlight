//
// Created by ryhor on 16/06/19.
//

#ifndef SUNLIGHT_FILE_TOOLS_H
#define SUNLIGHT_FILE_TOOLS_H
#include "headers.h"

template <class T>
void Write1DArrayToFile(const std::vector<T> &input_vector, const std::string &file_name) {
    std::ofstream fout(file_name, std::ios::binary);
    //int size = input_vector.size();
    //fout.write((char*)&size, sizeof(size));
    fout.write(reinterpret_cast<const char*>(&input_vector[0]), input_vector.size() * sizeof(T));
    fout.close();
}

template <class T>
void Write2DArrayToFile(const std::vector<std::vector<T>> &input_vector, const std::string &file_name) {
    std::ofstream fout(file_name, std::ios::binary);
    //int size = input_vector.size();
    //fout.write((char*)&size, sizeof(size));
    for (auto row: input_vector) {
        fout.write(reinterpret_cast<const char*>(&row[0]), row.size() * sizeof(T));
    }
    fout.close();
}

template <class T>
void Read1DArrayFromFile(const std::string &file_name, std::vector<T> &res) {
    std::ifstream fin(file_name, std::ios::binary);

    fin.seekg(0, std::ifstream::end);
    int size = fin.tellg() / sizeof(T);
    T temp2[size];
    fin.seekg(0, std::ifstream::beg);
    fin.read((char *) &temp2, sizeof(temp2));
    for (int counter = 0; counter < size; ++counter) {
        res.push_back(temp2[counter]);
    }
    fin.close();
    return res;
}

template <class T>
void Read2DArrayFromFile(const int &column_count, const std::string &file_name,
         std::vector<std::vector<T>> &res) {
    std::ifstream fin(file_name, std::ios::binary);

    fin.seekg(0, std::ifstream::end);
    int size = fin.tellg() / sizeof(T);
    T temp2[size];
    fin.seekg(0, std::ifstream::beg);
    fin.read((char *) &temp2, sizeof(temp2));
    int row = 0;
    int column = -1;
    if (res.size() == 0) {
        res.assign(static_cast<int>(size/column_count), std::vector<T>(column_count));
    }
    for (int counter = 0; counter < size; ++counter) {
        column++;
        if (column == column_count) {
            row++;
            column = 0;
        }
        res[row][column] = temp2[counter];
    }
    fin.close();
    return res;
}

#endif //SUNLIGHT_FILE_TOOLS_H
