//
// Created by ryhor on 30/01/19.
//

#include "algebra.h"

// CPP Program to find the number of
// transpositions in a permutation

// This function returns the size of a component cycle
void dfs(const std::vector<int> &perm, int i, std::vector<int> &visited, std::vector<int> &cycle)
{
	// If it is already visited
	if (visited[i] == 1)
		return;

	visited[i] = 1;
	cycle.push_back(i);
	dfs(perm, perm[i], visited, cycle);

}

// This functio returns the number
// of transpositions in the permutation
std::vector<std::vector<int>> PermutationToCycles(std::vector<int> perm) {
	// Initializing visited[] array
	std::vector<int> visited(perm.size());

	std::vector<std::vector<int>> cycles;

	// building the goesTo[] array
//	for (int i = 0; i < n; i++)
//		goesTo[P[i]] = i + 1;


	for (int i = 0; i < perm.size(); i++) {
		if (visited[i] == 0) {
		    std::vector<int> cycle;
			dfs(perm, i, visited, cycle);
			if (cycle.size()) {
			    cycles.push_back(cycle);
			}
		}
	}
	return cycles;
}


