//
// Created by ryhor on 05/07/19.
//

#include "input_output.h"

bool operator<(const Grid3D &cl, const Grid3D &cr) {
    if (cl.x < cr.x) return true;
    if (cl.x > cr.x) return false;

    if (cl.y < cr.y) return true;
    if (cl.y > cr.y) return false;

    if (cl.z < cr.z) return true;
    if (cl.z > cr.z) return false;

    return false;
}
