//
// Created by ryhor on 29/04/19.
//

#include "celestial_coordinate_system.h"
#include "file_tools.h"

using json = nlohmann::json;
typedef std::vector<std::vector<float>> HourDay;

void SaveSunPathDiagrammToBinary(const std::vector<HourDay> &solar_sin_altitude_north,
        const std::vector<HourDay> &solar_sin_altitude_southern, const std::vector<HourDay> &solar_cos_azimuth_north,
        const std::vector<HourDay> &solar_cos_azimuth_southern, const int hour_detalization, int max_latitude,
        const std::vector<std::vector<char>> &ksi_in_0_pi) {

    for (int latitude = 0; latitude < max_latitude; ++latitude) {
        std::vector<std::vector<float>> azim_altit_n(1, {0, 0});
        std::vector<std::vector<float>> azim_altit_s(1, {0, 0});

        int first_hour_n = -1, last_hour_n = -1, first_hour_s = -1, last_hour_s = -1;
        std::vector<std::vector<float>> first_last_hour_n(kPrecalculatedDays.size(), {-1, -1});
        std::vector<std::vector<float>> first_last_hour_s(kPrecalculatedDays.size(), {-1, -1});
        for (int hour = 0; hour < hour_detalization * 24; ++hour) {
            std::vector<float> row_azim_altit_n;
            std::vector<float> row_azim_altit_s;
            bool one_day_alt_posit_n = false, one_day_alt_posit_s = false;
            int day_ind = -1;
            for (auto day: kPrecalculatedDays) {
                day_ind++;
                if (solar_sin_altitude_north[latitude][hour][day] > 0) {
                    one_day_alt_posit_n = true;
                    if (static_cast<int>(first_last_hour_n[day_ind][0]) == -1) {
                        first_last_hour_n[day_ind][0] = hour;
                    }
                    first_last_hour_n[day_ind][1] = hour;
                }
                if (solar_sin_altitude_southern[latitude][hour][day] > 0) {
                    one_day_alt_posit_s = true;
                    if (static_cast<int>(first_last_hour_s[day_ind][0]) == -1) {
                        first_last_hour_s[day_ind][0] = hour;
                    }
                    first_last_hour_s[day_ind][1] = hour;
                }
            }
            if (one_day_alt_posit_n) {
                for (auto day: kPrecalculatedDays) {
                    row_azim_altit_n.push_back(solar_sin_altitude_north[latitude][hour][day]);
                    row_azim_altit_n.push_back(solar_cos_azimuth_north[latitude][hour][day]);
                }
                azim_altit_n.push_back(row_azim_altit_n);
                if (first_hour_n == -1) {
                    first_hour_n = hour;
                }
                last_hour_n = hour;
            }
            if (one_day_alt_posit_s) {
                for (auto day: kPrecalculatedDays) {
                    row_azim_altit_s.push_back(solar_sin_altitude_southern[latitude][hour][day]);
                    row_azim_altit_s.push_back(solar_cos_azimuth_southern[latitude][hour][day]);
                }
                azim_altit_s.push_back(row_azim_altit_s);
                if (first_hour_s == -1) {
                    first_hour_s = hour;
                }
                last_hour_s = hour;
            }
        }
        azim_altit_n[0][0] = static_cast<float>(first_hour_n);
        azim_altit_n[0][1] = static_cast<float>(last_hour_n);
        azim_altit_s[0][0] = static_cast<float>(first_hour_s);
        azim_altit_s[0][1] = static_cast<float>(last_hour_s);
        for (int i = 0; i < first_last_hour_n.size(); ++i) {
            azim_altit_n.push_back(first_last_hour_n[i]);
            azim_altit_s.push_back(first_last_hour_s[i]);
        }
        std::string file_name_n = "../../src/sunlight/precalculated_sunpathdiag/azim_altit_n_" + std::to_string(latitude);
        std::string file_name_s = "../../src/sunlight/precalculated_sunpathdiag/azim_altit_s_" + std::to_string(latitude);
        Write2DArrayToFile<float>(azim_altit_n, file_name_n);
        Write2DArrayToFile<float>(azim_altit_s, file_name_s);
    }
    std::vector<char> ksi_maxlat_hourdetal;
    ksi_maxlat_hourdetal.push_back(static_cast<char>(hour_detalization));
    for (int hour = 0; hour < hour_detalization * 24; ++hour) {
        std::vector<char> row_ksi_in_0_pi;
        for (auto day: kPrecalculatedDays) {
            row_ksi_in_0_pi.push_back(ksi_in_0_pi[hour][day]);
        }
        ksi_maxlat_hourdetal.insert(ksi_maxlat_hourdetal.end(), row_ksi_in_0_pi.begin(), row_ksi_in_0_pi.end());
    }
    Write1DArrayToFile(ksi_maxlat_hourdetal, "../../src/sunlight/precalculated_sunpathdiag/ksi_maxlat_hourdetal");
}



void SaveSunPathDiagrammToJSON(const std::vector<HourDay> &solar_sin_altitude_north,
        const std::vector<HourDay> &solar_sin_altitude_southern, const std::vector<HourDay> &solar_cos_azimuth_north,
        const std::vector<HourDay> &solar_cos_azimuth_southern, const int hour_detalization, int max_latitude,
        const std::vector<std::vector<char>> &ksi_in_0_pi) {
    json sunpath_diagram_json;
    sunpath_diagram_json["sin_altitude_n"] = solar_sin_altitude_north;
    sunpath_diagram_json["sin_altitude_s"] = solar_sin_altitude_southern;
    sunpath_diagram_json["cos_azimuth_n"] = solar_cos_azimuth_north;
    sunpath_diagram_json["cos_azimuth_s"] = solar_cos_azimuth_southern;
    sunpath_diagram_json["hour_detalization"] = hour_detalization;
    sunpath_diagram_json["max_latitude"] = max_latitude;
    sunpath_diagram_json["ksi_in_0_pi"] = ksi_in_0_pi;
    std::ofstream file("sunpath_diagram.json");
    file << sunpath_diagram_json;
    file.close();
}


void CalcCelestialCoordinateTable() {
    int year_day_count = 365;
    // 1. day angle
    const double pi = 3.14159;
    std::vector<int> J(year_day_count);
    std::vector<float> tau(year_day_count); // day angle
    std::vector<float> delta(year_day_count); // solar declination
    std::vector<float> ET(year_day_count); // equation of time

    for (int i = 0; i < year_day_count; ++i) {
        J[i] = i + 1;
        tau[i] = 2 * pi * (J[i] - 1) / 365;
        delta[i] = 0.006918 - 0.399912 * cos(tau[i]) + 0.070257 * sin(tau[i]) - 0.006758 * cos(2 * tau[i]) +
                   0.000907 * sin(2 * tau[i]) - 0.002697 * cos(3 * tau[i]) + 0.001480 * sin(3 * tau[i]);
        ET[i] = 0.170 * sin(4 * pi * (J[i] - 80) / 373) - 0.129 * sin(2 * pi * (J[i] - 8) / 355);
    }

    // 5. true solar time
    // and 6. Hour angle
    int hour_detalization = 2; // count
    int max_latitude = 81;
    std::vector<std::vector<float>> TST(hour_detalization * 24, std::vector<float>(year_day_count)); // equation of time
    std::vector<std::vector<char>> ksi_in_0_pi(hour_detalization * 24, std::vector<char>(year_day_count)); // equation of time
    auto ksi = TST;

    for (int time_interval = 0; time_interval < hour_detalization * 24; ++time_interval) {
        for (int day = 0; day < year_day_count; ++day) {
            float LT = static_cast<double>(time_interval) / hour_detalization;
            TST[time_interval][day] = LT + ET[day];
            ksi[time_interval][day] = (TST[time_interval][day] - 12) * 15 * pi / 180;
            if (ksi[time_interval][day] < 0) {
                ksi_in_0_pi[time_interval][day] = true;
            }
        }
    }

    std::vector<HourDay> solar_sin_altitude_north(max_latitude); //North hemisphere [latitude, day, hour_interval]
    std::vector<HourDay> solar_sin_altitude_southern(max_latitude); //Southern hemisphere [latitude, day, hour_interval]

    std::vector<HourDay> solar_cos_azimuth_north(max_latitude); //North hemisphere [latitude, day, hour_interval]
    std::vector<HourDay> solar_cos_azimuth_southern(max_latitude); //North hemisphere [latitude, day, hour_interval]

    std::vector<float> tmp_vect(365);
    for (int latitude = 0; latitude < max_latitude; ++latitude) {
        std::vector<std::vector<float>> hour_day_n_altit, hour_day_s_altit, hour_day_n_az, hour_day_s_az;
        for (int time_interval = 0; time_interval < hour_detalization * 24; ++time_interval) {
            hour_day_n_altit.push_back(tmp_vect);
            hour_day_s_altit.push_back(tmp_vect);
            hour_day_n_az.push_back(tmp_vect);
            hour_day_s_az.push_back(tmp_vect);
            for (int day = 0; day < year_day_count; ++day) {
                // ToDo latitude must be in radians
                hour_day_n_altit[time_interval][day] = sin(latitude * pi / 180) * sin(delta[day]) + cos(latitude * pi / 180) * cos(delta[day]) *
                                                                                   cos(ksi[time_interval][day]);
                hour_day_s_altit[time_interval][day] = sin(-latitude * pi / 180) * sin(delta[day]) + cos(-latitude * pi / 180) * cos(delta[day]) *
                                                                                    cos(ksi[time_interval][day]);
                auto sin_a = hour_day_n_altit[time_interval][day];
                auto cos_a = sqrt(1 - sin_a * sin_a);
                float res_n = kInf, res_s = kInf;
                if (cos_a > kEps) {
                    // if the sun isn't at its zenith, then azimuth is defined
                    res_n = (-sin(latitude * pi / 180) * sin_a + sin(delta[day])) /
                            (cos(latitude * pi / 180) * cos_a);
                    res_s = (-sin(-latitude * pi / 180) * sin_a + sin(delta[day])) /
                            (cos(-latitude * pi / 180) * cos_a);
                }
                hour_day_n_az[time_interval][day] = res_n;
                hour_day_s_az[time_interval][day] = res_s;

            }
        }
        solar_sin_altitude_north[latitude] = hour_day_n_altit;
        solar_sin_altitude_southern[latitude] = hour_day_s_altit;
        solar_cos_azimuth_north[latitude] = hour_day_n_az;
        solar_cos_azimuth_southern[latitude] = hour_day_s_az;
    }
    // save sunpath diagram
    SaveSunPathDiagrammToBinary(solar_sin_altitude_north, solar_sin_altitude_southern, solar_cos_azimuth_north,
        solar_cos_azimuth_southern, hour_detalization, max_latitude, ksi_in_0_pi);

}

std::vector<std::vector<int>> day_month_to_serial_number = {{1,  32, 60, 91,  121, 152, 182, 213, 244, 274, 305, 335},
                                                            {2,  33, 61, 92,  122, 153, 183, 214, 245, 275, 306, 336},
                                                            {3,  34, 62, 93,  123, 154, 184, 215, 246, 276, 307, 337},
                                                            {4,  35, 63, 94,  124, 155, 185, 216, 247, 277, 308, 338},
                                                            {5,  36, 64, 95,  125, 156, 186, 217, 248, 278, 309, 339},
                                                            {6,  37, 65, 96,  126, 157, 187, 218, 249, 279, 310, 340},
                                                            {7,  38, 66, 97,  127, 158, 188, 219, 250, 280, 311, 341},
                                                            {8,  39, 67, 98,  128, 159, 189, 220, 251, 281, 312, 342},
                                                            {9,  40, 68, 99,  129, 160, 190, 221, 252, 282, 313, 343},
                                                            {10, 41, 69, 100, 130, 161, 191, 222, 253, 283, 314, 344},
                                                            {11, 42, 70, 101, 131, 162, 192, 223, 254, 284, 315, 345},
                                                            {12, 43, 71, 102, 132, 163, 193, 224, 255, 285, 316, 346},
                                                            {13, 44, 72, 103, 133, 164, 194, 225, 256, 286, 317, 347},
                                                            {14, 45, 73, 104, 134, 165, 195, 226, 257, 287, 318, 348},
                                                            {15, 46, 74, 105, 135, 166, 196, 227, 258, 288, 319, 349},
                                                            {16, 47, 75, 106, 136, 167, 197, 228, 259, 289, 320, 350},
                                                            {17, 48, 76, 107, 137, 168, 198, 229, 260, 290, 321, 351},
                                                            {18, 49, 77, 108, 138, 169, 199, 230, 261, 291, 322, 352},
                                                            {19, 50, 78, 109, 139, 170, 200, 231, 262, 292, 323, 353},
                                                            {20, 51, 79, 110, 140, 171, 201, 232, 263, 293, 324, 354},
                                                            {21, 52, 80, 111, 141, 172, 202, 233, 264, 294, 325, 355},
                                                            {22, 53, 81, 112, 142, 173, 203, 234, 265, 295, 326, 356},
                                                            {23, 54, 82, 113, 143, 174, 204, 235, 266, 296, 327, 357},
                                                            {24, 55, 83, 114, 144, 175, 205, 236, 267, 297, 328, 358},
                                                            {25, 56, 84, 115, 145, 176, 206, 237, 268, 298, 329, 359},
                                                            {26, 57, 85, 116, 146, 177, 207, 238, 269, 299, 330, 360},
                                                            {27, 58, 86, 117, 147, 178, 208, 239, 270, 300, 331, 361},
                                                            {28, 59, 87, 118, 148, 179, 209, 240, 271, 301, 332, 362},
                                                            {29, 59, 88, 119, 149, 180, 210, 241, 272, 302, 333, 363},
                                                            {30, 59, 89, 120, 150, 181, 211, 242, 273, 303, 334, 364},
                                                            {31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365}};