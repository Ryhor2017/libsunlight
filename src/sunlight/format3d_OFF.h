//
// Created by ryhor on 28/08/18.
//

#ifndef BIDIRECTIONAL_LIST_TEST_FORMAT3D_OFF_H
#define BIDIRECTIONAL_LIST_TEST_FORMAT3D_OFF_H

#include <vector>
#include <map>
#include "./geometry2d.h"
#include "./geometry3d.h"
#include <fstream>
#include <string>
#include <iostream>

struct Face {
    Face(){};
    Face(std::vector<int> vert_ind_, std::vector<double> color_) {
        vert_ind = vert_ind_;
        color = color_;
    }
    Face(std::vector<int> vert_ind_) {
        vert_ind = vert_ind_;
    }
    int vert_count = 4;
    std::vector<int> vert_ind;
    std::vector<double> color = {0, 255, 0, 1};
};

class FormatOFF {
public:
    FormatOFF(){};
    FormatOFF(std::vector<PolygonByPoints> all_contours_, std::vector<double> wall_heights_){
        all_contours = all_contours_;
        wall_heights = wall_heights_;
        for (auto cont: all_contours) {
            wall_point_count += cont.points.size();
        }
        // wall_point_count *= 2;
        wall_points.assign(wall_point_count * 2, Point3d(0, 0, 0));
    };
    std::vector<Point3d> wall_points;
    std::vector<Face> wall_faces;
    std::vector<Point3d> panel_points;
    std::vector<Face> panel_faces;
    std::vector<PolygonByPoints> all_contours;
    int wall_point_count = 0;
    std::vector<double> wall_heights;

    void CreateOFFfile(const std::vector<Point3d> &wall_points, const std::vector<Face> &wall_faces,
                       const std::string &file_name) {

        std::ofstream out(file_name);
        out << "OFF" << std::endl;
        out << wall_points.size() << " " << wall_faces.size() << " " << 0 << std::endl;
        for (auto point: wall_points) {
            out << point.x << " " << point.y << " " << point.z << std::endl;
        }
        for (auto face: wall_faces) {
            out << face.vert_ind.size() << " ";
            for (auto ind: face.vert_ind) {
                out << ind << " ";
            }
//            out << face.color[0] << " " << face.color[1] << " " << face.color[2] << " " << face.color[3] << std::endl;
            out << face.color[0] << " " << face.color[1] << " " << face.color[2] << std::endl;
        }
        out.close();
    }

    void CreateOFFfile(const std::string &file_name) {
        CreateOFFfile(wall_points, wall_faces, file_name);
    }

    void CreateWalls(std::string file_name = "walls.off", std::vector<double> color_ = {0, 255, 0}) {
        int base = 0;
        std::map<int, int> begin_ind_inner_cont;
        Vector3d vect_z = {0, 0, 1};
        int ind = -1;
        for (auto contour: all_contours) {
            ind++;
            for (int i = 0; i < contour.points.size(); ++i) {
                wall_points[i + base] = Convert3D(contour.points[i]);
                wall_points[i + base + wall_point_count] = wall_points[i + base] + vect_z * wall_heights[ind];
                int i_next = i + 1;
                if (i == contour.points.size() - 1) {
                    i_next = 0;
                }
                wall_faces.push_back(Face({i + base, i_next + base, i_next + base + wall_point_count,
                                           i + base + wall_point_count}, color_));
            }
            begin_ind_inner_cont[base + wall_point_count] = 1;
            base += contour.points.size();
        }
        // each inner contour must be closed (use first point)
        std::vector<int> accumul_len(all_contours.size());
        std::map<int, std::pair<int, int>> out_link_inn;// out_point_id, <inn_cont_id, inn_point_id>
        if (all_contours.size() > 1) {
            for (int i = 1; i < all_contours.size(); ++i) {
                accumul_len[i] = all_contours[i - 1].points.size() + accumul_len[i - 1];
                int min_inn_point;
                int min_out_point;
                double min_dist = Infin;
                for (int j = 0; j < all_contours[i].points.size(); ++j) {
                    for (int k = 0; k < all_contours[0].points.size(); ++k) {
                        if (Dist2d(all_contours[i].points[j], all_contours[0].points[k]) < min_dist) {
                            min_dist = Dist2d(all_contours[i].points[j], all_contours[0].points[k]);
                            min_inn_point = j;
                            min_out_point = k;
                        }
                    }
                }
                out_link_inn[min_out_point] = std::pair<int, int>(i, min_inn_point);
            }
        }

        std::vector<int> processed_inn_cont(all_contours.size());
        Face roof_face({}, color_);
        int next_point = wall_point_count;
        for (int j = 0; j < all_contours[0].points.size(); ++j) {
            roof_face.vert_ind.push_back(j + wall_point_count);
            if (out_link_inn.find(j) != out_link_inn.end() && !processed_inn_cont[out_link_inn[j].first]) {
                for (int i = out_link_inn[j].second; i < all_contours[out_link_inn[j].first].points.size(); ++i) {
                    roof_face.vert_ind.push_back(accumul_len[out_link_inn[j].first] + i + wall_point_count);
                }
                for (int i = 0; i < out_link_inn[j].second + 1; ++i) {
                    roof_face.vert_ind.push_back(accumul_len[out_link_inn[j].first] + i + wall_point_count);
                }
                processed_inn_cont[out_link_inn[j].first] = 1;
            }
            roof_face.vert_ind.push_back(j + wall_point_count);
        }
        // roof_face.vert_ind.push_back(wall_point_count);
        roof_face.vert_count = roof_face.vert_ind.size();
        wall_faces.push_back(roof_face);
        CreateOFFfile(wall_points, wall_faces, file_name);

    }

    void CreateEnvelop(const int grid_cont_size, const PolygonByPoints &grid, const std::vector<double> &heights,
            std::string file_name = "envelop.off") {
        int base = 0;
        std::map<int, int> begin_ind_inner_cont;
        Vector3d vect_z = {0, 0, 1};
        int ind = -1;
        // walls
        for (int j = 0; j < grid_cont_size; ++j) {
            int j_next = j + 1;
            if (j == grid_cont_size - 1) {
                j_next = 0;
            }
            int j_up = j + grid.points.size();
            int j_next_up = j_next + grid.points.size();
            wall_points[j] = Convert3D(grid.points[j]);
            wall_points[j_next] = Convert3D(grid.points[j_next]);
            wall_points[j_up] = wall_points[j] + vect_z * heights[j];
            wall_points[j_next_up] = wall_points[j_next] + vect_z * heights[j_next];
            wall_faces.push_back(Face({j_up, j, j_next, j_next_up}));
        }
        //roof
        int center = wall_points.size() - 1;
        wall_points[center] = Convert3D(grid.points[grid.points.size() - 1]) +
                vect_z * heights[grid.points.size() - 1];
        for (int j = grid.points.size(); j < grid.points.size() + grid_cont_size; ++j) {
            int j_next = j + 1;
            if (j_next == grid.points.size() + grid_cont_size) {
                j_next = grid.points.size();
            }
            int j_inn = j + grid_cont_size;
            int j_inn_next = j_next + grid_cont_size;
            wall_points[j_inn] = Convert3D(grid.points[j_inn - grid.points.size()]) +
                    vect_z * heights[j_inn - grid.points.size()];

            wall_points[j_inn - grid.points.size()] = Convert3D(grid.points[j_inn - grid.points.size()]);

            wall_points[j_inn_next] = Convert3D(grid.points[j_inn_next - grid.points.size()]) +
                    vect_z * heights[j_inn_next - grid.points.size()];

            wall_points[j_inn_next - grid.points.size()] = Convert3D(grid.points[j_inn_next - grid.points.size()]);

            wall_faces.push_back(Face({j_inn_next, j_next, j}));
            wall_faces.push_back(Face({j_inn, j_inn_next, j}));
            wall_faces.push_back(Face({center, j_inn_next, j_inn}));
        }
        CreateOFFfile(wall_points, wall_faces, file_name);
    }
    void AddParallelepiped(Point2d begin, Point2d fin, double base_z, double height, double width, std::vector<double> color) {
        Vector3d vect_z = {0, 0, 1};
        Vector3d norm_ort_vec = Convert3D(OrtVectOut(Norm(Vector2d(fin.x - begin.x, fin.y - begin.y))), 0);
        Point3d p1 = Convert3D(begin, base_z);
        Point3d p2 = Convert3D(fin, base_z);
        Point3d p3 = p2 + norm_ort_vec * width;
        Point3d p4 = p1 + norm_ort_vec * width;
        Point3d p5 = p1 + vect_z * height;
        Point3d p6 = p4 + vect_z * height;
        Point3d p7 = p3 + vect_z * height;
        Point3d p8 = p2 + vect_z * height;
        int base_ind = panel_points.size();
        panel_points.insert(panel_points.end(), {p1, p2, p3, p4, p5, p6, p7, p8});
//        Face face1({base_ind, base_ind + 1, base_ind + 2, base_ind + 3}, color);
//        Face face2({base_ind + 3, base_ind + 5, base_ind + 4, base_ind}, color);
//        Face face3({base_ind + 2, base_ind + 6, base_ind + 5, base_ind + 3}, color);
//        Face face4({base_ind + 1, base_ind + 7, base_ind + 6, base_ind + 2}, color);
//        Face face5({base_ind + 6, base_ind + 7, base_ind + 4, base_ind + 5}, color);
//        Face face6({base_ind + 7, base_ind + 1, base_ind + 0, base_ind + 4}, color);
        Face face1({base_ind + 3, base_ind + 2, base_ind + 1, base_ind + 0}, color);
        Face face2({base_ind + 0, base_ind + 4, base_ind + 5, base_ind + 3}, color);
        Face face3({base_ind + 3, base_ind + 5, base_ind + 6, base_ind + 2}, color);
        Face face4({base_ind + 2, base_ind + 6, base_ind + 7, base_ind + 1}, color);
        Face face5({base_ind + 5, base_ind + 4, base_ind + 7, base_ind + 6}, color);
        Face face6({base_ind + 4, base_ind + 0, base_ind + 1, base_ind + 7}, color);
        panel_faces.insert(panel_faces.end(), {face1, face2, face3, face4, face5, face6});
    };

    void CreatePanels() {
         CreateOFFfile(panel_points, panel_faces, "panels.off");
    }

    void CreatePanels(std::string file_name) {
        CreateOFFfile(panel_points, panel_faces, file_name);
    }

};

#endif //BIDIRECTIONAL_LIST_TEST_FORMAT3D_OFF_H
