#include <iostream>
#include <string>
#include "../sunlight/celestial_coordinate_system.h"
#include "../sunlight/solver.h"
#include "../sunlight/geometry2d.h"
#include <chrono>
#include <cxxopts/cxxopts.hpp>

using json = nlohmann::json;




int main(int argc, char** argv) try {
	cxxopts::Options opt(argv[0]);
	opt.add_options()
		("in", "Input file", cxxopts::value<std::string>(),
			"This option takes one argument which specifies "
			"the name of the input file.")
			("out", "Output file", cxxopts::value<std::string>(),
				"This option takes one argument which specifies "
				"the name of the output file.");
	auto cmd = opt.parse(argc, argv);
	if (cmd.count("in") && cmd.count("out")) {

		    Input input;
            ReadJSONInput(cmd["in"].as<std::string>(), input);

            SunPathDiagramV2 sun_path_diag;

            InitSunPathDiagramV2(input, sun_path_diag, "../");

            input.PrepareInput(sun_path_diag);

            // ------------------- site's height task
            std::vector<double> site_envelop;

            bool is_north = true;
            PolygonByPoints site_grid;
            CalculateSiteEnvelop(sun_path_diag, input, site_grid, site_envelop);

            Prepare3DVisualization(input, sun_path_diag, site_grid, site_envelop, cmd["out"].as<std::string>());

//		try {
//			std::ofstream out;
//			out.exceptions(std::ofstream::failbit | std::ofstream::badbit);
//			out.open(cmd["out"].as<std::string>());
//			auto j = json(result);
//			out << j.dump();
//		}
//		catch (std::ofstream::failure& e) {
//			std::cerr << "Exception open/writing file: " << e.what() << '\n';
//			return 1;
//		}
	}
	else {
		std::cout << "Error in command line:\n   "
			"You must specify in and out options.\n";
		std::cout << "\nTry the -h option for more information." << std::endl;
	}
	return 0;
} catch (const std::exception& e) {
	std::cout << e.what() << std::endl;
}








