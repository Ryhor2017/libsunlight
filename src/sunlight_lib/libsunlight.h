//
// Created by ryhor on 7/7/18.
//

#ifndef PACKING2D_PACKER_H
#define PACKING2D_PACKER_H

#ifdef PACKERLIB_EXPORTS
#ifdef WINDOWS
        #define PACKER_API extern "C" __declspec(dllexport)
#else
        #define PACKER_API extern "C"  __attribute__((visibility("default")))
#endif
#else
#ifdef WINDOWS
        #define PACKER_API extern "C" __declspec(dllimport)
#else
        #define PACKER_API extern "C"
#endif
#endif


// src - serialized InInfo object, dst - serialized OutInfo object
PACKER_API void sunlight_gen(const char* src, int src_size,
	char** dst, int* dst_size);

// MATCHER_API
PACKER_API void free_data(char** range);


#endif //PACKING2D_PACKER_H
