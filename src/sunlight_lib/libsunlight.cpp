//
// Created by ryhor on 7/7/18.
//

#include "./libsunlight.h"
#include "../sunlight/solver.h"
#include "../sunlight/proto/converter.h"
#include "../../third_party/json/json.hpp"

using json = nlohmann::json;

void sunlight_gen(const char* src, int src_size,
	char** dst, int* dst_size)
{
	*dst_size = 0;
	*dst = nullptr;
	auto data = [&]() {
		prosunlight::InInfo proto;
		proto.ParseFromArray(src, src_size);
		Input input;
		Convert(proto, input);
		return input;
	}();
//	const auto config_horiz = [&]() {
//		Config result;
//		try {
//			std::ifstream in;
//			in.exceptions(std::ifstream::failbit | std::ifstream::badbit);
//			in.open(data.horiz.config_path);
//			json j;
//			in >> j;
//			ReadConfig(j, result);
//		} catch (std::ifstream::failure& e) {
//			std::cerr << "Exception open/reading file: " << e.what() << '\n';
//		}
//		return result;
//	}();


    // SOLVER
    Output result;
    Solver(data, result);

    // OUTPUT

	{
		prosunlight::OutInfo proto;
		Convert(result, proto);
		*dst_size = proto.ByteSize();
		*dst = new char[*dst_size];
		proto.SerializeToArray(*dst, *dst_size);
	}
	google::protobuf::ShutdownProtobufLibrary();
}

extern void free_data(char** range) {
	delete[] * range;
	*range = nullptr;
}