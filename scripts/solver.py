import copy
import json
import math
import os


def Solve(input):
    test_dir = "./test_files/"
    test_out_dir = "./test_out_files/"
    if not os.path.isdir(test_dir):
        os.mkdir(test_dir)
    if not os.path.isdir(test_out_dir):
        os.mkdir(test_out_dir)
    input_path = test_dir + "input.json"
    with open(input_path, 'w') as f:
        json.dump(input, f)
	
    for the_file in os.listdir(test_out_dir):
        file_path = os.path.join(test_out_dir, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
    output_path = test_out_dir
    os.system("sunlight.exe " + " --in " + input_path + " --out " + output_path)
    #os.system("sunlight.exe")
    print("Sun envelop was calculated, see " + output_path)

