import solver

input = {

    "latitude": 51,
    "days": [0], #0 - летн.солнцест., 1 - зимн.солнцстоян., 2 - весенн.равнод, 3 - осенн.равнод
"site": [ # стройплощадка
[-30,-10], [-30, 10], [-20,10], [-20,40], [25,40], [25, -10]
],

# соседние здания
"neighb_buildings": [
[[-35,15], [-35,45], [-25,45], [-25,15]],
[[-25,45], [-25,55], [35,55], [35,45]],
[[30,-15], [30,25], [40,25], [40,-15]]
],
"neighb_builds_heights": [20, 60, 40],
# высота окон первого жилого этажа
"neighb_windows_heights": [0, 0, 0],
# не изменять
"buildings": [
[[-30,-20], [-30,15], [15,30], [20,15], [25,-10], [15, -15], [5, 10], [-15,5], [-15,-20]]
],
"buildings_heights": [20],
    "win_heights": [[3]],
}
solver.Solve(input)
